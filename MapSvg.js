// SVG taken from https://github.com/flekschas/simple-world-map
// And converted to React Native using https://www.smooth-code.com/open-source/svgr/playground/

import React from 'react';

import { Svg } from 'expo';
const { Path, G } = Svg;

const SvgComponent = props => (
    <Svg
        viewBox="30.767 241.591 784.077 458.627"
        {...props}
    >
        <Path
            style={props.selectedCountries.get('_somaliland') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M512.674 502.797l3.526 2.403 1.046-.052 8.757-3.008.994 3.206-.701 2.706-1.893 1.503-4.729-.302-6.769-4.158-.231-2.298z"
        />
        <Path
            style={props.selectedCountries.get('AE') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M528.466 468.135l.753 3.008 8.522.752.596-6.172 1.644-.897.448-2.257-2.688.753-2.99 4.521-6.285.292z"
        />
        <Path
            style={props.selectedCountries.get('AF') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M545.85 435.383l1.374 10.771 3.423.752.32 1.937-2.455 2.049 4.573 3.691 8.885-3.198.709-3.786 5.593-3.491 2.145-8.091 1.599-1.722-1.659-2.887 5.412-3.347-.691-.968-2.498.155-.226 2.299-3.354-.033-.062-3.068-1.079-1.288-1.815 1.649.052 1.515-2.739 1.036-5.059-.319-6.568 6.881-5.88-.537z"
        />
        <Path
            style={props.selectedCountries.get('AL') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M450.679 420.438v3.984l1.142 2.152.82-.096 1.409-2.566-.821-1.15-.319-2.844-1.089-1.012-1.142 1.532z"
        />
        <Path
            style={props.selectedCountries.get('AM') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M507.47 420.549l4.149 5.411-1.218 1.427-2.939-.51-3.646-3.268.196-2.146 3.458-.914z"
        />
        <G style={props.selectedCountries.get('AO') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }>
            <Path
                className="prefix__mainland"
                d="M437.366 547.461l2.948 11.003-.069 3.478-4.313 4.633-.647 7.527 16.597.147 5.395 1.954 4.45-.58-2.594-3.251.01-9.282 5.101-.217v-3.621l-4.142-.172-.829-8.575-1.746.024-.943-.848-1.027.054-1.365 2.646h-5.255l-1.22-1.227.363-1.738-1.436-2.101-9.278.146z"
            />
            <Path d="M435.577 544.453l1.504 1.953 1.945-1.842-.571-1.909-.483-.035-2.395 1.833z" />
        </G>
        <G style={props.selectedCountries.get('AR') ? props.countrySelectedStyle : props.selectedContinents.get('SA') ? props.continentSelectedStyle : props.countryStyle }>
            <Path
                className="prefix__mainland"
                d="M279.05 600.613l1.677 1.571-6.371 9.467-2.239 2.479.777 10.813 4.918 5.974-4.132 7.209-3.129 1.35h-3.579l1.003 5.627-5.593 1.92 1.34 4.729-3.354 10.701 4.141 3.38-2.239 5.515-3.804 5.975 2.014 4.165-4.918.786-4.028-4.951-.674-15.432-6.258-26.209 1.893-9.163-4.028-11.713 2.68-15.204 2.463-2.931-.605-2.222 3.164-2.888 7.054.483 3.942 4.21 4.555.078 4.668 2.853-1.375 3.217.329 3.25 6.612-.312 3.096-4.727z"
            />
            <Path d="M264.745 687.564l.225 4.951 3.803-.337 3.242-2.144-5.48-1.124-1.79-1.346z" />
        </G>
        <Path
            style={props.selectedCountries.get('AT') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M430.46 403.459l-.562 1.167.483.83 2.015-.415h1.711l1.857 1.572 3.95-.717 2.904-1.729.743-1.167-.112-1.504-2.611-1.954-3.501.035-.294 1.988-3.683 1.797-2.9.097z"
        />
        <G style={props.selectedCountries.get('AU') ? props.countrySelectedStyle : props.selectedContinents.get('OC') ? props.continentSelectedStyle : props.countryStyle }>
            <Path
                className="prefix__mainland"
                d="M672.961 609.067l-.303 21.938-3.371 2.472-.303 2.161 4.598 3.086 11.35-2.161h5.826l2.145-3.095 12.879-2.472 9.198 2.784-.614 3.708 1.228 3.708 7.055-1.236.303 1.851-4.599 3.396 1.53 1.236 3.37-1.236-.917 10.2 6.44 4.944 3.683-1.236 1.841 1.852 10.735-1.548 10.123-16.381 3.682-.925 7.357-13.596 1.841-11.739-4.599-5.869 1.842-1.236-3.684-11.436-3.984-2.783.614-15.448-3.684-2.782-.916-8.652h-1.842l-6.138 20.392-3.37.312-7.668-7.728 4.297-11.437-7.971-1.547-8.896 2.472-2.454 7.104-3.984.925-.304-4.944-16.252 9.889.304 3.708-2.454 3.397h-6.139l-13.19 5.56-4.605 12.392z"
            />
            <Path d="M728.775 668.089l-1.531 6.181.304 4.322 4.599-.312 5.213-8.03-8.585-2.161z" />
        </G>
        <Path
            style={props.selectedCountries.get('AZ') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M508.931 418.674l-.873 1.486 4.071 5.342 1.418-.458 2.334 2.446 1.011-4.287 2.533.406-.104-1.229-4.165-3.646-.795 2.143-5.43-2.203z"
        />
        <Path
            style={props.selectedCountries.get('BA') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M442.708 411.084l-.319.527 5.801 5.981 2.127-3.13-.078-1.234-1.857-2.256-5.674.112z"
        />
        <Path
            style={props.selectedCountries.get('BD') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M616.256 457.908l-1.134 2.049 2.938 5.584.087 4.357.535 1.166 3.449.061 1.953-1.875 1.418.855.285 2.652 1.133-.708.069-3.389-.951-.112-.596-2.879-2.403-.086-.596-1.6 1.469-1.962.024-.97h-4.269l-3.411-3.143z"
        />
        <Path
            style={props.selectedCountries.get('BE') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M414.019 391.704l-.554 1.383 5.947 3.925.4.051.375-1.094.837-.588-1.336-1.499h-.916l-1.255-1.426-3.498-.752z"
        />
        <Path
            style={props.selectedCountries.get('BF') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M404.493 493.496l3.146-.25 5.16 7.295-4.789 3.613-3.466-.892-4.66.062-.752 2.73-3.907.19-1.071-1.461 1.385-4.442 8.954-6.845z"
        />
        <Path
            style={props.selectedCountries.get('BG') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M457.092 414.066l.139 4.305 1.452 3.025 5.454.095 2.455-1.737 2.412-.959-.588-2.75.545-1.469-1.228-.641-1.687.139-1.323 1.332-5.549.043-2.082-1.383z"
        />
        <Path
            style={props.selectedCountries.get('BI') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M478.504 538.385l3.691-.078-.959 3.232-.935.812h-1.142l-.812-2.187.157-1.779z"
        />
        <Path
            style={props.selectedCountries.get('BJ') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M411.512 515.288h1.833l.104-5.204 2.315-3.363-.104-5.852-2.102-.052-3.604 2.816 1.504 2.87.054 8.785z"
        />
        <Path
            style={props.selectedCountries.get('BN') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M689.038 515.08l-2.489 3.018 2.04.641 1.149-1.608-.7-2.051z"
        />
        <Path
            style={props.selectedCountries.get('BO') ? props.countrySelectedStyle : props.selectedContinents.get('SA') ? props.continentSelectedStyle : props.countryStyle }
            d="M238.631 561.361l7.114-3.104 2.351.226 1.565 6.534 10.839 3.604 1.79 5.523 4.469.562 1.902 4.729-1.34 4.278-7.27.562-2.68 6.872-5.705-.112-1.79-.337-3.293 3.197-1.625-.156-5.593-12.957 1.547-2.316.545-9.163-1.383-5.455-1.443-2.487z"
        />
        <Path
            style={props.selectedCountries.get('BR') ? props.countrySelectedStyle : props.selectedContinents.get('SA') ? props.continentSelectedStyle : props.countryStyle }
            d="M286.631 618.464l5.402-10.391.198-8.73 10.079-6.501h5.645l4.435-7.512.804-14.418-1.815-3.855 10.684-9.751.406-10.761-14.514-7.105-17.53-5.48-8.264-.812 2.221-4.669-.604-7.104-1.808-.596-2.671 5.308-1.4 1.754-3.595-1.59-12.093 4.261-4.028-5.073.648-5.299-3.804 3.872-4.201-2.265-.424.597.009 1.841 3.622 1.945-5.437 5.73-3.432-.034-3.475-3.535-3.933.121-.484 4.2 2.256 2.739-2.663 8.532-3.112.241-4.953 3.13-1.21 6.146 4.296 4.599.787-.89 3.017-.812 2.576 4.34 7.374-3.164 2.861.165 1.971 6.976 10.52 3.337 1.815 5.564 4.478.537 2.135 5.314-1.443 4.729 1.884 2.474-.276 3.683 5.048-.477 4.625 5.844-.363 4.105 2.74 2.316-6.57 9.95 11.566 6.474z"
        />
        <G style={props.selectedCountries.get('BS') ? props.countrySelectedStyle : props.selectedContinents.get('NA') ? props.continentSelectedStyle : props.countryStyle }>
            <Path d="M222.121 463.112l-1.089-.337-.086 2.101 1.34 1.349.917-1.349-1.082-1.764zM224.29 466.397l-1.504.838 1.417 2.021.752-1.011-.665-1.848zM229.14 467.91l-1.591-.087.165 1.012 1.167 1.687 1.002-1.099-.743-1.513zM228.388 465.896l-2.593-1.099-.501-2.609 1.002-.425 1.003 2.023 1.002.76.087 1.35zM225.881 460.588l-1.34-.337-.251-1.686-1.417-.501.917-.926 1.668.588 1.253.762-.83 2.1z" />
        </G>
        <Path
            style={props.selectedCountries.get('BT') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M616.108 453.561l1.34 1.833 4.528.034-.458-2.507-5.41.64z"
        />
        <Path
            style={props.selectedCountries.get('BW') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M454.56 594.589l1.858.57-.26 5.314 1.911.26 4.391-3.959 5.273.57 1.399-3.545 6.673-6.095-8.013-9.223-.104-1.514-.883-.26-2.43 2.24-6.31.154-.884 7.866-2.479.57-.142 7.052z"
        />
        <Path
            style={props.selectedCountries.get('BY') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M456.418 382.861l1.297 2.135-.519 1.703.086 1.349.476 1.616 2.68-1.521 3.329.086 2.334.959h5.922l1.729-4.141 1.037-1.564v-1.045l-3.717-5.23-3.285-1.305-2.68-.303-2.335.743.088 2.351-3.241 4.098-3.201.069z"
        />
        <Path
            style={props.selectedCountries.get('BZ') ? props.countrySelectedStyle : props.selectedContinents.get('NA') ? props.continentSelectedStyle : props.countryStyle }
            d="M191.823 483.228l-.043 3.154h.726l2.472-4.615h-1.677l-1.478 1.461z"
        />
        <G style={props.selectedCountries.get('CA') ? props.countrySelectedStyle : props.selectedContinents.get('NA') ? props.continentSelectedStyle : props.countryStyle }>
            <Path
                className="prefix__mainland"
                d="M151.767 281.182l1.72 2.602.864 3.475 4.305 1.081 3.017-3.25 2.585 1.306 7.321.647 5.169-2.17.864 7.157h3.017v-3.034l3.017.216 7.538 8.895 4.953 3.035-2.584 4.123 1.081 1.081 9.673 1.953.216 4.34 2.585.433.648-6.51 4.089-1.08 3.017 4.556 6.457 3.034 3.233.647 2.152-2.602.216-4.124 3.873-2.387 1.288 3.476-3.449 6.077.432 3.033 1.937-3.033 3.873-3.475.216-4.556-2.152-3.476.648-2.817 5.169-2.603 2.369 1.738.432 15.188 3.657-3.25 2.152 1.305-3.017 5.204 3.873.865 5.602-8.679 4.737 4.986-1.936 8.895-4.737 2.603-4.521-2.169-8.177 1.736.864 2.818-2.152 3.476-6.673 1.521-7.538 5.86-6.673 8.896-.864 2.817 4.521 1.737 1.72 4.339 6.241 6.293 9.906 4.339-2.152 9.975-.216 2.818 2.585 1.736 3.449-4.555.432-8.679 5.385-.216 2.584-4.988.433-7.589 6.889-13.45 8.609 3.034 4.521 6.293-1.937 6.292 3.449 1.954 8.394-5.646 2.368 15.404 7.754 9.327.216 4.771-8.609 2.17-4.088 4.338-8.609-1.953-4.305-.217-7.538 5.86 4.521-1.081 5.601-1.08 1.081 1.305-1.504 4.771.216 4.34 2.585 1.736 2.584-.648 1.296-1.953h1.72l-2.801 5.205-5.385.215-2.368 3.476h-3.017l-.864-2.603 4.305-4.338-5.169 1.737-.233-7.374-1.487-.863-4.521 1.953-.432 3.691h-10.338l-8.832 6.08-11.842 3.908-1.288-1.738 5.964-8.902-3.388-3.261-2.152-4.132-4.383-3.346-4.702-.389-8.428-5.904-61.122-10.043-1.011-4.142-5.602-5.204v-4.339l.865-3.907-.433-2.168-2.152-2.171-.432-3.475 5.602-3.908-3.449-18.653-4.737-.217-4.305-5.645 23.649-40.18z"
            />
            <Path d="M130.684 350.117l-1.47 2.818.51 1.996.959.598-.225.812-1.029.294.294 2.965 1.106 1.115.882-.959-1.106-2.887.657-2.299 1.616-2.152-1.175-1.997-1.019-.304zM135.542 367.008l-1.323.52 2.429 2.818.588 3.336 2.429 2.592 2.058-.371v-3.406l-2.498-1.557-3.683-3.932zM268.15 295.833l-1.53 1.547 1.34 2.126 6.328.77-4.028-4.252-2.11-.191zM191.105 270.143l.19 3.475-6.898 7.148 1.729 5.791 4.979-1.348 2.878-4.253 7.278-2.706 5.938-.389-4.599-5.022-2.299 1.737-1.729-.579-.959-2.126-2.109-2.126-4.399.398zM200.113 259.908l-1.53 2.706 7.477 2.706 2.68-4.055 1.15 2.707h1.919l3.639-4.055-4.409-1.158-1.729-1.348-2.299 2.316-6.898.181zM213.148 265.318l-5.938 2.508v1.928l7.667 2.896-1.729 1.928 1.15 2.507 4.789-2.126h4.028l1.919 3.086 3.259-3.285-.77-3.095-2.68.968-.38-3.863 1.34-2.316h-1.34l-2.109 1.349-.959.769.579 2.707-1.53 1.158-2.299-.19-.579-3.476-4.418-3.453zM221.005 259.329l-.579 1.927 3.639 1.738 2.68-1.547-.19-1.158-5.55-.96zM223.875 256.044l-2.679.968.19 1.35 5.938-.389-.19-1.349-3.259-.58zM236.72 259.329l-.38 1.348-.959 1.349v1.929l3.639-.579 3.83 3.285h1.34v-3.285l-3.83-4.253-3.64.206zM246.497 263.191l1.53 1.738-1.34 2.316.959 2.507 4.218-2.317v-1.736l-2.49-2.896-2.877.388zM252.055 258.75l.19 3.086h5.178l1.34 1.158-.19 1.348-4.599.58 3.259 4.443 4.409.77 6.128-2.705-8.817-13.33-2.68 1.738.19 2.316-3.069-1.158-1.339 1.754zM207.399 280.576l-7.278 1.928-4.219 3.673.38 4.054 7.667 2.317-1.729 3.864-5.558-3.477-1.53 2.896 3.639 2.507-.19 4.054 5.558 1.547 6.708-.389 1.149-2.126 4.979 5.601 3.449-1.157.579-3.864 2.49 1.737.38-3.864-3.068-1.928.19-12.162-2.68-2.126-2.87 3.864-8.046-6.949zM230.782 289.073l-2.489-1.158-1.34 1.737 2.68 4.253.19 4.054 5.749-3.475v-5.022l2.109-2.126-2.109-1.547h-3.449l-1.341 3.284zM243.048 287.335l-4.028 3.285.959 4.054h2.49l1.149-2.126 1.729 1.737 1.729-.19 4.599-3.864-8.627-2.896zM242.659 280.956l-.959 1.928 4.218 1.548 1.15-1.738-4.409-1.738zM240.169 273.617l-4.218.579-2.49 2.315 4.599.19-1.34 3.475.959 1.548 1.34-.19 3.259-5.212-2.109-2.705zM247.456 272.27l-2.299.77.38 3.086 3.83 2.507.19 1.927-1.149 1.159.579 3.864 14.755 4.823 4.028 1.349 4.028-3.475-4.789-3.864-4.409 1.158-6.128-.579-2.299-2.316-.579-6.371-3.83-1.928-2.308-2.11z" />
            <Path d="M259.523 292.357l-4.218-.39-4.979 1.927-2.68 3.666.769 10.043 8.238.39 7.857 3.864 5.559 6.371 4.218-.19-1.15 5.981-3.829 6.371-4.218 1.926-3.069-.578-1.53-1.348-2.299 3.086.959 3.086 3.259.189 4.028-1.928 3.449 8.886 8.626 5.603 5.938-7.529-4.979-8.108 2.878-3.284 4.028 6.758 7.278-6.371-1.339-2.896-4.98 1.548-3.448-9.466 3.259-5.403-6.518-6.949-3.639 2.507-3.449-7.529-7.277.968-1.919-9.076-5.938 4.055-.579 5.021h-3.259l.38-4.443 4.573-6.758zM262.021 274.006v1.738l-4.218.968 1.149 1.927 4.789 1.928 5.368.58 3.831 2.705 3.829-2.127-2.681-2.705h3.449l2.109-2.316 5.178-.77v-1.158l-2.878-1.928.379-2.127 8.048 1.348 11.886-4.633-4.41-1.348 1.15-1.547h9.197l1.531-1.547-18.593-6.569-4.41-1.547-4.789 3.476-5.368-4.443-2.878-.19-.579 3.675-3.638-3.285-4.218 1.349.769 2.126 6.328 1.35-.38 3.086 3.449 2.125 8.438-2.125.19 2.896-6.898 3.285-4.218-3.285-3.83.389 3.83 5.411-1.919.969-2.878-2.508-2.109 1.348 1.919 3.666h3.259l-.769 3.475-2.68-.389-3.449-3.674-2.285.401zM244.941 327.159l-3.657 4.599-.225 5.065 3.198-1.841h3.881l2.74 2.533 2.515-2.076-8.452-8.28zM289.466 386.977l-9.136 8.748.916 2.074 11.186 4.141 1.599-2.758-.916-4.599-3.657.458-2.057-2.299 3.423-3.449-1.358-2.316z" />
        </G>
        <Path
            style={props.selectedCountries.get('CD') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M438.023 546.597l8.912-.155 1.808 2.567-.069 1.893.665.605h4.426l1.271-2.499h1.808l.734.742 2.479-.069.735 8.714 4.287.139v.675l11.521 5.195.536 1.012h2.411l-.268-3.648-4.356-2.092.269-2.766 1.876-4.392 4.287-.139-3.683-12.224.068-5.195 5.826-9.11.068-1.278-.874-.477.035-2.472-1.062-.095-1.072-1.366-17.59-.795-3.226 3.138-5.28-3.475-1.858 1.141-1.348 11.35-3.337 2.575-1.003 2.283.179 3.381-6.017 4.918-1.6-.726.216.94-1.774 1.705z"
        />
        <Path
            style={props.selectedCountries.get('CF') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M443.452 519.229l4.028 4.356 1.59-2.057 2.532.104.544-2.006 2.49-1.556 5.169 3.562 2.982-2.956 11.574.51-10.736-11.082 1.443-.897.198-1.954-2.438-1.149h-3.58l-5.766 5.715-.197 2.351-4.573-.146-.146 1.003-2.982-.305-2.688 5.108.556 1.399z"
        />
        <Path
            style={props.selectedCountries.get('CG') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M439.424 526.551l-.052 1.255 4.131.104.146 10.728-3.777-.104-2.187-1.703-1.693.951-.078.476.873.423.251 2.204-2.334 2.006.501 1.057 2.585-2.006h1.244l.396 1.2 1.644.7 5.271-4.46-.104-3.259 1.099-2.653 3.38-2.507.907-8.479-2.402.009-2.783 3.812-7.018.246z"
        />
        <Path
            style={props.selectedCountries.get('CH') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M423.787 402.82l-3.771 4.011.078.405 1.547-.483 1.393 1.937 2.352-.83 1.625 1.263.667-.38 2.005-3.146-.511-.484-1.979-.051-.959-1.963-2.447-.279z"
        />
        <Path
            style={props.selectedCountries.get('CI') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M388.484 521.562l3.697-2.617 4.6-.806 4.693 1.013-2.395-3.622-.701-2.213.701-6.544-4.192.198-1.9-1.814-3.994.104-1.901.304.198 4.425-1.002.406-1.203 2.215 3.095 3.62.304 5.331z"
        />
        <G style={props.selectedCountries.get('CL') ? props.countrySelectedStyle : props.selectedContinents.get('SA') ? props.continentSelectedStyle : props.countryStyle }>
            <Path d="M261.391 683.51l-3.691 8.109 6.371.674.112-5.403-2.792-3.38zM260.137 682.239l-2.775 3.068-.337 3.604-5.368-3.043-5.705-8.221-1.677-2.932 2.351-3.043-.225-3.83-2.68-1.124-2.126-1.572.449-2.144 2.792-.787.562-12.387-4.356-2.48-2.844-64.477.735-1.278 5.567 12.836 1.781.035.579 2.049-2.369 2.868-2.723 15.448 3.873 11.895-1.79 9.007 6.31 26.485.666 15.49 4.521 5.229 4.789-.696z" />
            <Path d="M241.717 649.833l-1.115 1.686.562 2.932 1.115.112.562-3.718-1.124-1.012z" />
        </G>
        <Path
            style={props.selectedCountries.get('CM') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M428.031 519.428l2.783 2.561-.199 3.959 15.266-.354 1.245-1.399-4.375-4.711-.648-1.703 2.784-5.212-1.893-3.458-1.591-.855v-1.755l1.841-1.202.104-5.463-1.46-.164-.024 2.87-6.414 11.972-3.925.199-2.688 1.85-.806 2.865z"
        />
        <G style={props.selectedCountries.get('CN') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }>
            <Path
                className="prefix__mainland"
                d="M594.498 386.128l-2.99 7.521-4.124-.217-4.349 9.518 3.691 4.701-7.606 10.504-3.907-.658-2.611 3.285.648 1.971 3.043.217 1.521 3.5 3.044.658 9.344 12.04v6.129l4.563 2.844 4.996-.873 6.303 3.719 7.605 2.187 3.691-.439 4.132-.441 8.687-5.688 2.828.44 1.08 2.567 2.396.718 3.26 4.813-2.17 4.814 1.306 3.285 3.69 1.312.647 3.942 4.35.439.647-1.971 6.302-3.285 3.907.217 4.563 5.03 3.043-1.312 1.954.216.873 2.412 1.521.216 2.169-3.06 8.688-3.285 7.823-9.413 2.61-8.974-.217-5.912-3.259-.656 1.953-2.188-.434-3.501-8.255-8.314v-4.157l2.386-3.062 2.388-1.098.216-2.412h-6.085l-1.089 3.285-2.828-.656-3.475-3.718 2.17-5.688 3.043-3.285 2.827.217-.434 5.031 1.521 1.313 3.691-3.717 1.306-.216-.433-2.844 3.476-4.158 2.61.216 1.521-4.813 1.781-.942.182-3-1.729-1.815-.147-4.736 3.329-.217-.216-12.214-2.334 1.4-.864 3.13-3.897-.009-11.298-6.354-8.16-9.837-8.281-.086-2.107 1.833 2.68 6.137-.935 5.758-3.335 1.383-1.876-.147-.139 5.696 1.954.441 3.476-1.53 4.563 2.187v2.188l-3.26.216-2.611 5.688-2.386.215-8.472 11.16-8.902 3.941-6.086.441-4.124-2.844-5.869 3.068-6.302-1.971-1.521-4.158-10.642-.657-5.646-9.188h-2.386l-1.92-4.262-2.287-.181z"
            />
            <Path d="M671.802 472.655l-2.064.579-1.487 1.833 1.236 2.411 1.814.163 2.066-1.832.492-2.411-2.057-.743z" />
        </G>
        <Path
            style={props.selectedCountries.get('CO') ? props.countrySelectedStyle : props.selectedContinents.get('SA') ? props.continentSelectedStyle : props.countryStyle }
            d="M234.326 498.251l-1.781-.182-11.773 9.707-1.245 3.414-1.608.182.717 7.546-4.105 10.07 4.46 3.776 5.714.363 3.924 5.757 5.705.183-.182 4.312h2.135l2.316-7.909-2.144-2.694.536-5.031 4.46-.363-.536-11.688-9.993-3.232-2.316-6.293 5.716-7.918z"
        />
        <Path
            style={props.selectedCountries.get('CR') ? props.countrySelectedStyle : props.selectedContinents.get('NA') ? props.continentSelectedStyle : props.countryStyle }
            d="M202.905 502.745l1.202 2.352.977 1.297-1.314 3.898-2.507-1.764-4.097-3.752v-2.48l5.739.449z"
        />
        <Path
            style={props.selectedCountries.get('CU') ? props.countrySelectedStyle : props.selectedContinents.get('NA') ? props.continentSelectedStyle : props.countryStyle }
            d="M205.904 469.846v1.1l4.599.086 2.169-1.263.337.926 4.512 1.098 4.011 3.622-.917 1.262.165 1.436 3.345.839 3.345-1.514 1.504-1.513-2.169-1.098-11.194-6.569-3.924-.424-5.783 2.012z"
        />
        <G style={props.selectedCountries.get('CV') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }>
            <Path d="M350.01 490.264l-1.642.942 1.175.941 1.409-.708-.942-1.175zM354.046 492.165l-1.071.951.762 1.409 1.832-.821-1.523-1.539zM351.704 494.836l-1.375.821 1.479 1.979 1.168-.612-1.272-2.188z" />
        </G>
        <Path
            style={props.selectedCountries.get('CY') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M484.555 437.794l1.062.771-3.294 3.119-1.573-.052-1.167-.821.156-1.529 2.386-.155 2.43-1.333z"
        />
        <Path
            style={props.selectedCountries.get('CZ') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M437.202 398.921h3.83l2.049 1.461 3.795-3.155-3.683-2.627-3.648-1.765-2.498.45-3.389 2.178 3.544 3.458z"
        />
        <Path
            style={props.selectedCountries.get('DE') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M422.257 384.234l3.086-.5v-2.178l2.584-.425 1.418 1.427 1.495.164 2.334-1.012 2.083.588 1.832 1.592.251 5.955 1.832 2.438-2.411.337-4.003 2.515.338.839 3.579 3.354-.251 1.677-3.328 1.677-3.085.086-.753 1.59h-1.581l-.752-1.676-2.749-.675-.088-2.767-1.39-.77.114-1.861-.406-1.328-1.982-1.824.414-2.854 2.161-1.011-.742-5.358z"
        />
        <G style={props.selectedCountries.get('DK') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }>
            <Path d="M427.123 370.076l-3.586 3.968-.13 2.584 1.635 4.263 2.559-.484-.319-3.483 1.764-1.971-.034-1.548-1.245-3.223-.644-.106z" />
            <Path d="M428.979 377.354l-1.062.229v1.583l1.128.875.997-.25-.243-1.503-.82-.934zM432.306 375.848l-.949.23-1.056.968.448 1.954 1.292.507-1.334.535-.255.685h2.005l.602-1.099-.768-.378.25-.962.916-1.205-.25-1.042-.901-.193z" />
        </G>
        <Path
            style={props.selectedCountries.get('DJ') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M508.991 499.771l-.493 2.903 3.424-.052.052-4.271-1.253-.77-1.73 2.19z"
        />
        <Path
            style={props.selectedCountries.get('DM') ? props.countrySelectedStyle : props.selectedContinents.get('NA') ? props.continentSelectedStyle : props.countryStyle }
            d="M256.23 485.371l-.761 1.616.917 1.228 1.141-.994-1.297-1.85z"
        />
        <Path
            style={props.selectedCountries.get('DO') ? props.countrySelectedStyle : props.selectedContinents.get('NA') ? props.continentSelectedStyle : props.countryStyle }
            d="M242.434 481.533l-4.573-2.991-2.887-1.021-.578 5.521.578-.047.761 1.461.994-1.15 2.896-.77 2.516.536.293-1.539z"
        />
        <Path
            style={props.selectedCountries.get('DZ') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M424.625 435.764l-3.526-1.186-14.677 2.758-3.198 2.429 1.953 10.088-5.835.233-3.509 5.646-8.359 2.005.025 4.105 27.532 21.048 4.692.398 15.654-12.231-1.565-1.971-2.938-.398-1.764-2.955V453.5l-1.177-1.184.199-3.155-3.13-3.155-.389-3.354 1.366-.986-.59-3.553-.764-2.349z"
        />
        <G style={props.selectedCountries.get('EC') ? props.countrySelectedStyle : props.selectedContinents.get('SA') ? props.continentSelectedStyle : props.countryStyle }>
            <Path
                className="prefix__mainland"
                d="M213.986 529.43l-4.088 2.541-.294 3.771-.821 1.234 2.576 2.473-1.115 1.219.259 3.113 4.607 1.098 6.976-8.256-.017-2.878-3.345-.216-4.738-4.099z"
            />
            <Path d="M183.533 531.443l-.536 2.378-.994 1.002.683 1.228 1.754-.69.838-1.461-.536-1.537-1.209-.92z" />
        </G>
        <G style={props.selectedCountries.get('EE') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }>
            <Path
                className="prefix__mainland"
                d="M462.562 363.299l-4.84-.173-3.068 1.875-.043 1.392 1.987 1.875 6.182 1.047-.218-6.016z"
            />
            <Path d="M452.236 364.042l-1.308.44 1.308.226.595.69.711-.852-.709-1.215-.597.711zM452.792 365.792l-1.862.833-.643 1.111.643.722 2.362-.875 1.137-.752-1.637-1.039z" />
        </G>
        <Path
            style={props.selectedCountries.get('EG') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M466.16 449.222l2.308.062 4.495 1.244 2.135.061 2.646-2.213h1.234l2.249 1.245h2.845l.51-.035 1.798 5.17.51 1.668.477 2.498-.85.622-1.461-.734-1.686-5.498-1.521-.111-.111 1.867 1.012 3.232 8.1 10.027.173 4.305-2.359 2.723-22.163-.251-.341-25.882z"
        />
        <Path
            style={props.selectedCountries.get('ER') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M496.224 493.859l-.216-5.093 3.423-3.992.926.709 1.686 5.637 8.091 6.023-1.47 1.808-5.922-5.091-6.518-.001z"
        />
        <G style={props.selectedCountries.get('ES') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }>
            <Path
                className="prefix__mainland"
                d="M402.565 416.322h-11.014l-2.222-1.004-1.071.078-1.297 2.696.458 2.775 4.21.389.536 1.771-1.833 10.33.078 1.851 2.981 1.617 3.439.232 6.881-1.694 3.363-4.233.077-4.313 5.965-5.395.302-2.386-5.428-.078-5.425-2.636z"
            />
            <Path d="M374.265 458.444l-1.513.873.7.709.813-1.582zM369.009 458.608l-1.875.476.935 1.418h1.407l-.467-1.894zM364.549 457.191l-1.176 1.184 1.643 1.418.935-2.126-1.402-.476zM413.578 426.877l-1.375.467.304 1.235h1.988l.839-.925-1.756-.777z" />
        </G>
        <Path
            style={props.selectedCountries.get('ET') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M489.982 508.606l6.292-14.005 6.25.035 5.541 4.814-.39 3.968h4.296l.44 2.386 6.95 4.157 4.286.217-8.15 8.756-11.194 3.449h-2.773l-4.944-4.219-1.953-.82-3.786-5.576-2.499.035-.294-2.559 1.928-.638z"
        />
        <Path
            style={props.selectedCountries.get('FI') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M453.072 340.202l1.79.786 1.104 2.074-1.104 1.436-5.55 6.068-.952 3.199 1.271 4.633 4.278 3.199 5.706-2.715 4.598-.64 4.279-6.872-3.173-7.512-3.018-7.192.477-4.633-1.901-.32-.492-3.38-2.559-4.175-2.836 1.962-1.114 4.556-3.008-1.807-4.185-1.021-.934 1.09 1.607 1.453 2.931-.053 2.359 3.812.426 6.052z"
        />
        <G style={props.selectedCountries.get('FK') ? props.countrySelectedStyle : props.selectedContinents.get('SA') ? props.continentSelectedStyle : props.countryStyle }>
            <Path d="M281.194 678.393l-2.273-.25-2.265 1.521 1.642 1.781 2.896-3.052zM283.459 677.252l-.752 2.411-2.144 1.901.13.631 3.655-1.4 1.513-1.901-2.402-1.642z" />
        </G>
        <G style={props.selectedCountries.get('FR') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }>
            <Path
                className="prefix__mainland"
                d="M412.973 393.588l-1.91.467-3.82 4.158-1.149.078-1.53-1.081-.993.233-.762 2.386-5.584.155.156 1.236 3.82 2.543 4.435 3.543-.077 4.236-2.368 4.157 5.126 2.464 5.204.154 1.606-1.85 3.286.078.916.848 3.285-.233 1.686-2.162-2.145-2.541-.155-1.616.458-1.771-1.071-1.539-1.833.535-.232-1.383 4.054-4.469v-2.697l-2.348-.767-1.432-.987-6.623-4.175z"
            />
            <Path d="M276.163 517.285l5.058 3.154-2.646 5.255-.959 1.21-2.809-1.615.079-5.663 1.277-2.341zM540.023 586.93l-1.972.129-.129 1.722 1.313.269 1.972-.925-1.184-1.195zM516.857 562.666l.656 1.461h1.055l.526-1.857-2.237.396zM258.823 489.822l-.917.847.683 1.383 1.296-.38-1.062-1.85zM428.039 418.016l-1.687 1.695-.154 1.538 1.374.847.536-.076.303-2.24-.372-1.764zM254.095 484.065l-1.296.535.458 1.149 1.521-.994-.303-.311-.38-.379z" />
        </G>
        <Path
            style={props.selectedCountries.get('GA') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M435.438 526.646l-.104 2.151-4.875-.104-2.982 5.766 7.012 7.667 1.736-1.453-.052-1.503-1.192-.554v-1.057l2.688-1.701 2.387 1.807 2.638.052-.054-9.067-4.176-.196-.052-1.903-2.974.095z"
        />
        <G style={props.selectedCountries.get('GB') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }>
            <Path
                className="prefix__mainland"
                d="M400.629 367.984l-1.582 2.395.631.959h3.648v1.6l-.952 1.278.632 3.354 2.058 3.994 1.581 3.672 2.533.959 1.105 1.92-.155 1.755-1.582.959-.156.795 1.106.64-.951 1.279-2.221.959-4.279-.477-6.664 3.035-2.221-1.115 6.345-3.674-.796-.477-3.328-.32 2.058-3.033.319-2.56 2.696-.319-.475-4.953-3.174-.155-.95-1.115.155-3.674-1.901.156 1.901-6.388 3.492-2.56 1.127 1.111z"
            />
            <Path d="M393.974 378.693l-2.853.32-.156 2.56 1.901 1.278 2.058-.475.796-1.436-1.746-2.247z" />
        </G>
        <Path
            style={props.selectedCountries.get('GE') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M495.144 415.596l2.827 3.691 3.527 1.625 2.169-.01 3.726-1.01.935-1.461-11.021-4.123-2.163 1.288z"
        />
        <Path
            style={props.selectedCountries.get('GH') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M399.091 513.179l.968 2.273 2.524 3.959 1.4-.052 3.819-2.171-.268-12.354-2.957-.864-4.141.112-1.345 9.097z"
        />
        <G style={props.selectedCountries.get('GL') ? props.countrySelectedStyle : props.selectedContinents.get('NA') ? props.continentSelectedStyle : props.countryStyle }>
            <Path
                className="prefix__mainland"
                d="M292.587 282.398l-1.176 1.877 2.118 2.117-.942 2.118 3.06 3.994 3.761-1.176 4.936-.466 5.644 6.111 3.761 10.104-3.051 6.345 4.227-.708 2.354 1.409.232 3.06-5.168.233 2.818 2.817 3.526.709-7.754 10.339-.942 6.345 1.643 5.169-1.176 3.06 2.118 6.578 3.993 4.469 1.176-.232 2.584-.709.233 3.76 1.642 2.352 3.052-.234 2.353-8.695 7.053-8.697 10.581-4.227 6.578-8.229 3.05 1.408h6.345l5.17-5.168 6.346-2.584.708-3.994-3.993-3.527-3.526-1.175-1.885-4.937 4.469-2.584 7.054 3.76 2.353-2.584-3.761-2.117 7.996-10.813-1.409-4.702-3.761-.232 1.408-4.228 4.703-2.118 9.637-8.461-2.816-3.053-10.814.941-5.645 5.646 3.295-7.287-3.762-.942-2.117 3.761-3.051-2.585-8.464.941 2.353-3.76 13.865-.467-3.527-4.702-15.04-2.817-6.11.941.233 3.061-6.345-2.118.232-2.118-4.47.941-.942 2.353 4.703 1.642-4.936 3.527-3.527-3.994-4.936-1.408-.709 3.76h-4.937l-1.885-3.994-7.754-1.176-4.228 2.117-.233 2.818-5.402-.709-3.294 1.409.234 3.293v1.644l-6.111 1.176-2.818-1.877-1.885 3.052 2.819 3.06 5.878-.709.467 1.885-4.469 2.118-3.99-1.907z"
            />
            <Path d="M311.396 319.066l1.409 2.119-.708 2.584h-1.409l-1.885-2.119.467-1.643 2.126-.941zM370.159 313.189l3.993 1.176-.234 3.293-4.227-2.118-.942-1.176 1.41-1.175z" />
        </G>
        <Path
            style={props.selectedCountries.get('GM') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M366.719 497.006l-.111.959 5.98-.086.304-.891-.13-.898-1.721.7-4.322.216z"
        />
        <Path
            style={props.selectedCountries.get('GN') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M369.77 505.304l2.629 4.046 3.423-2.974 3.511-.155 2.922 3.881 2.48 1.635.933-1.816.83-.466-.061-3.993-1.65-4.737-5.065.562-6.267-.501-.034 1.606-3.651 2.912z"
        />
        <G style={props.selectedCountries.get('GQ') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }>
            <Path d="M427.184 522.134l-.396 1.703 1.191.648 1.143-.855-.397-1.755-1.541.259zM430.771 526.697l-.054 1.202 3.924.198-.052-1.356-3.818-.044z" />
        </G>
        <G style={props.selectedCountries.get('GR') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }>
            <Path
                className="prefix__mainland"
                d="M453.004 427.213l-.096 1.15 4.003 2.014 1.911.734-1.003 1.055-2.23.227-.319 1.01.771 1.738 2.498 1.33 1.09.096.138-2.981 1.635-1.972-4.46-5.272.589-1.789 1.046-.043 1.591 1.279 1.002-.501.319-1.79 3.732.534 1.134-3.239-1.953 1.375-5.731-.14-3.726 1.929-1.941 3.256z"
            />
            <Path d="M461.69 438.442l1.408.043.589.873h2.05l1.363-.501.459.553-.907 1.192-4.002.139-.728-.959-.77-.458.538-.882z" />
        </G>
        <Path
            style={props.selectedCountries.get('GT') ? props.countrySelectedStyle : props.selectedContinents.get('NA') ? props.continentSelectedStyle : props.countryStyle }
            d="M183.456 491.11l5.126 3.752 5.169-6.423-.881-1.331-1.764-.062v-3.761l-1.322-.804-4.002 1.192 1.53 3.526-3.856 3.911z"
        />
        <Path
            style={props.selectedCountries.get('GW') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M368.196 502.356l1.211 2.395 3.396-2.922.035-.899-4.003-.579-.639 2.005z"
        />
        <Path
            style={props.selectedCountries.get('GY') ? props.countrySelectedStyle : props.selectedContinents.get('SA') ? props.continentSelectedStyle : props.countryStyle }
            d="M261.399 510.654l6.241 5.652-2.481 2.87-.199 1.703 3.259 3.362-.078 3.232-5.67 2.16-3.397-4.59.726-5.515-1.452-4.105 3.051-4.769z"
        />
        <Path
            style={props.selectedCountries.get('HN') ? props.countrySelectedStyle : props.selectedContinents.get('NA') ? props.continentSelectedStyle : props.countryStyle }
            d="M194.408 488.742l7.987-.303 2.369 2.817-1.478-.338-2.844.121-3.717 3.492-1.59 3.536-1.046-.555-.009-3.872-2.299-1.539 2.627-3.359z"
        />
        <Path
            style={props.selectedCountries.get('HR') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M443.417 407.816l-3.051 2.515h-3.095l-.372 2.178 1.418.372.709-1.055 1.114.977.891 3.112 6.109 2.853.605-.691-6.197-6.396.631-1.166 5.888-.226.596-1.876-3.838.111-1.408-.708z"
        />
        <Path
            style={props.selectedCountries.get('HT') ? props.countrySelectedStyle : props.selectedContinents.get('NA') ? props.continentSelectedStyle : props.countryStyle }
            d="M231.845 477.159l2.974.311-.354 3.648-.294 1.919-3.466-.19-.614.926-1.063-.077-.38-1.997 3.656-.304-.225-2.073-1.677-.691 1.443-1.472z"
        />
        <Path
            style={props.selectedCountries.get('HU') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M444.386 403.01l-1.003 1.573.078 2.403 1.599.82 4.919.147 6.854-5.774.034-1.279-.742-.371-4.953 2.248-6.786.233z"
        />
        <G style={props.selectedCountries.get('ID') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }>
            <Path d="M639.517 513.628l-.241 1.971 5.869 9.862h1.711l12.23 20.461 4.894.492 2.445-7.148-3.916-2.464-.734-3.941-22.258-19.233zM697.475 540.891l1.954 2.396-1.271 3.596v.684h2.887l1.021-8.989.934.26 1.694 8.212 1.615.434 1.53-3.512-1.53-5.308-1.271-2.31 3.993-2.911-.935-1.288-3.819 2.48h-1.021l-1.866-2.74.597-1.201 3.146-1.539 4.754 1.453 1.444-.089 3.568-3.335-1.442-1.451-3.312 2.565h-2.127l-3.224-1.538-2.29.086-2.55 4.106-1.616 7.105-.863 2.834zM718.791 524.805l-1.616 3.935 2.549 3.337h.849l1.105-2.223.597-.77-1.105-1.201-1.617-.597-.762-2.481zM723.805 537.729l-3.482.77-1.021 1.115.847 1.453 2.291-.855 1.442-.855 2.126 1.712.935-.771-1.693-2.057-1.445-.512zM666.045 548.854l-2.377 1.625.51 1.364 7.564 1.712 3.82.684 1.615 1.712 4.331.345 2.04 1.711 1.867-.432 1.702-1.539-3.146-1.452-2.715-2.308-7.053-1.713-8.158-1.709zM690.768 556.295l-1.865 1.029 1.104 1.201 2.715-1.027-1.954-1.203zM693.991 555.526l.338 1.625 1.954.51.761-.942-.848-1.288-2.205.095zM698.668 559.805l-2.377.347 2.126 1.798h1.694l-1.443-2.145zM699.342 556.979l-.511 1.027 3.821.596 2.974-1.711-1.694-.511-2.715.771-1.021-.856-.854.684zM711.833 557.583l-4.416 3.683.423.942 1.866-.345 2.205-2.059 4.331-.597-.848-1.452-3.561-.172zM734.126 532.446l-3.604.406-2.315 1.693.959 1.938 3.925.726v.727l-2.48 2.015 1.201 4.192 1.201.078 1.037-4.115h1.919l.806 4.027 9.361 7.746.241 6.051 3.198 3.467 1.442-.077.319-21.369-5.438-3.785-5.125 3.467-1.843 1.132-3.043-1.937-.078-6.128-1.683-.254zM690.689 519.532l-1.997 7.503-10.831 3.656-3.241-3.804-1.573.433 2.939 11.34 4.398.493 5.869 2.222v2.222l2.688-.493 3.916-5.42v-4.435l2.204-4.435 2.445.491-2.938-6.163-.449-3.968-3.43.358z" />
        </G>
        <Path
            style={props.selectedCountries.get('IE') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M394.915 383.085l-.786 5.187-6.975 2.56h-2.223l-1.582-1.115v-.96l3.492-2.238-.951-1.92.156-2.714 3.018.155 1.383-3.25-.183 2.887 2.344 1.858 2.307-.45z"
        />
        <Path
            style={props.selectedCountries.get('IL') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M486.378 444.899l-1.365 4.348 1.771 5.213 2.031-7.616v-1.633l-2.437-.312z"
        />
        <Path
            style={props.selectedCountries.get('IN') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M595 509.688l3.958-1.938 2.352-8.505-.104-10.441 13.468-14.54v-3.447l2.774-1.081-.104-3.984-2.991-5.817 1.712-3.121 3.742 3.449 4.808.216v1.938l-1.495 1.616.318.863 2.567.104.536 2.904h.752l1.928-3.449.958-9.042 3.207-2.265.104-3.12-1.279-2.481-2.031-.104-7.951 5.256.5 3.38-5.584-.019-1.97-2.41-1.072.138.363 3.354-12.075-.863-7.485-3.337-.397-4.106-4.988-3.094-.06-6.371-3.423-3.916-7.867.752.855 3.424 3.854 3.12-6.665 13.641-4.46.337-.734 1.644 4.392 4.062-.216 4.105-4.486-.069-.484 2.04 3.727-.164.104 1.616-2.671 1.4 1.711 3.232 3.312 1.08 2.031-1.504.959-2.688 1.177-.535 1.392 1.398-.424 3.449-.96 1.617.217 2.8L595 509.688z"
        />
        <Path
            style={props.selectedCountries.get('IQ') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M502.793 433.637l-1.348 6.664-5.585 4.65.354 2.195 5.455.371 8.688 7.07 4.857-.138.13-1.634 1.78-1.91 2.49 1.409.329-.312-4.815-6.405-2.282-.139-3.033-3.898.604-2.868.926-.121.319-1.271-4.132-4.348-4.737.685z"
        />
        <Path
            style={props.selectedCountries.get('IR') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M507.409 427.516l-1.057 1.098.104 1.738 1.314 1.842 4.658 5.101-.709 2.04h-.812l-.406 2.04 2.637 3.371 2.43.207 4.865 6.732 2.732.208 2.126 1.529.104 3.062 8.411 4.9h3.139l1.927-1.634 2.43-.104 1.418 3.268 9.085 1.262.27-3.337 3.007-1.089.139-1.193-2.395-3.268-5.334-4.287 2.801-2.55-.198-1.124-3.511-.544-1.486-11.843-.173-2.723-9.518-3.641-4.218.951-2.36 2.896-2.092-.139-.604.511-4.66-.303-5.878-4.288-2.188-2.394-1.003.24-1.808 2.066-3.187-.601z"
        />
        <Path
            style={props.selectedCountries.get('IS') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M366.261 340.521l-1.693-.959-2.283 1.443-1.962 1.814.052 1.013 2.541.319-.156 1.815-.898.908.217.588 2.541.164v2.938l3.656.641 2.17 1.229 2.437.104 4.186-2.083 3.231-4.271.053-2.887-1.963-1.66-1.642-1.392-.743.536-1.115 1.443-1.271-.164-1.271-1.393-1.642.156-2.386 1.979-1.437 1.547-.795-.69-.053-1.712.795-.536-.569-.89z"
        />
        <G style={props.selectedCountries.get('IT') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }>
            <Path
                className="prefix__mainland"
                d="M423.233 409.391l-.535 1.356.146 1.478 2.065 2.412 3.25-.113 7.175 8.334 4.479 1.297 2.646 2.498.631 5.695 1.417-.828 1.229-3.104-.303-2.229 2.101-.19.304-1.263-5.922-2.834-5.619-5.523-2.238-3.303-.545-3.137 2.861-.684-.734-2.066-1.755-1.478-1.513-.069-2.108.58-1.99 2.781-1.201.795-1.858-1.141-1.983.736z"
            />
            <Path d="M440.668 431.898l-1.253-.674-4.278.674.146 1.158 3.847 1.937.579.631 1.012.147-.053-3.873zM427.806 423.566l-2.289 1.158.303 4.469 1.833.311 1.374-1.312v-4.235l-1.221-.391z" />
        </G>
        <Path
            style={props.selectedCountries.get('JM') ? props.countrySelectedStyle : props.selectedContinents.get('NA') ? props.continentSelectedStyle : props.countryStyle }
            d="M221.533 480.798l-3.008.761v.84l1.755 1.011h1.841l1.167-1.349-1.755-1.263z"
        />
        <Path
            style={props.selectedCountries.get('JO') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M489.473 447.251l-2.126 7.416-.096 1.133h3.346l3.743-3.303.094-1.253-1.529-1.564 2.739-2.272-.396-2.109-.752.173-2.282 1.635-2.741.144z"
        />
        <G style={props.selectedCountries.get('JP') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }>
            <Path d="M709.317 426.193l-1.41 1.418.579 1.996 1.236.086.83 4.332.993 1.08 1.738-1.582.151-4.773-2-2.125-2.117-.432zM716.688 422.188l-2.659 2.156-.591 2.719 1.812 1.25 2.625-2.75.37-3.062-1.557-.313z" />
            <Path d="M713.613 418.033l-4.219 4.832v2.322l2.604-.312 4.085-3.592 2.731-.502.663.779.015 2.377.688 1.25h1.255l1.763-2.158.743-2.836 3.552-.086 3.476-4.166-1.814-6.916-.83-3.664 1.815-1.496-4.133-6.241-.944-.744-1.875.744-.481 2.584v2.083l.994 1.167.328 5.498-2.56 3.164-1.485-.917-1.159 2.584-.251 2.412.909 1.418-.579 1.08-1.902-1.582h-1.322l-1.157.666-.91.252zM720.729 380.396l-1.321 1.168.665 2.498 1.158 1.166-.086 3.83-1.487.578-1.158 2.584 3.388 4.659 2.23-.752.415-1.167-2.396-2.161 1.487-1.919 1.572.25 3.43 2.305.37-2.584 1.63-2.979 2.281-2.312-2.469-1.125-.944-1.801-1.236.83-1.071 1.331-2.316-.501-2.396-1.582-1.746-2.316zM733.201 377.812l-2.317 3.25.164 1.582 1.158-.502 2.723-3.414-1.728-.916zM736.261 373.066l-.829 2.248.086 1.496 1.409-.918 1.322-2.662v-.994l-1.988.83z" />
        </G>
        <Path
            style={props.selectedCountries.get('KE') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M491.142 521.365l2.3 4.484-2.759 5.783-.361 1.754 13.77 8.516 4.271-6.708-2.161-1.754-.043-8.835 2.705-2.956-4.313 1.435-3.259.044-5.1-4.305-1.608-.692-2.98.276-.526.883.064 2.075z"
        />
        <Path
            style={props.selectedCountries.get('KG') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M565.463 411.316l-.268 2.188.216 1.35 7.521 2.523-6.604 2.662-.751-.623-1.427.917.068.501.761.346 4.635.121 2.351-.709 3.018-3.803 3.776.656 4.557-6.311-12.188-1.66-1.686 4.088-2.127-2.281-1.852.035z"
        />
        <Path
            style={props.selectedCountries.get('KH') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M655.076 497.982l3.535 3.776 6.576-4.875.579-7.692-3.396 2.343-1.764-.985-2.396-.32-1.341-.941-.648.035-1.754 2.878.285 1.332 1.781.994-.216 2.705-1.241.75z"
        />
        <Path
            style={props.selectedCountries.get('KM') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M514.359 560.013l.396 1.321 1.711.269.656-1.72-2.763.13z"
        />
        <Path
            style={props.selectedCountries.get('KP') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M687.751 407.047l1.59.666.484 5.566 3.155.183 2.974-3.483-1.029-.916.121-3.734 2.731-3.303-1.392-2.506.908-1.039.501-2.592-1.582-.719-1.35.684-1.668 5.064-2.697-.232-3.12 3.682.374 2.679z"
        />
        <Path
            style={props.selectedCountries.get('KR') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M696.446 410.443l5.342 4.356.909 4.22-.183 2.264-2.61 2.939-2.248.12-2.551-5.506-.968-2.629 1.028-.795-.242-1.099-1.271-.569 2.794-3.301z"
        />
        <Path
            style={props.selectedCountries.get('KW') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M519.2 452.774l-1.945-1.056-1.349 1.356.146 2.715 3.139 1.201.009-4.216z"
        />
        <Path
            style={props.selectedCountries.get('KZ') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M513.495 402.163l3.544-1.513 3.959-.139.276 6.051h-2.317l-1.772 2.888 2.317 3.847 3.414 1.928.311 2.205 1.255-.416 1.157-1.375 1.91.416.96 1.928h2.455v-2.473l-1.504-4.4-.684-3.569 4.364-1.929 5.869.959 3.684 3.709 8.323-.821 4.644 6.597 5.455.274 1.504-2.472 1.91-.416.274-2.748 2.862-.139 1.503 1.789 1.505-3.57 12.957 1.789 2.179-2.887-3.683-4.537 4.91-10.719 3.958.275 2.731-6.594-5.455-.554-3.138-3.024-8.644 1.002-11.134-10.762-3.926 3.482-11.902-5.402-14.601 7.148-.405 5.083 3.413 3.985-6.655 3.76-8.636-.19-1.807-2.653-6.769-.373-6.414 4.123-.14 5.638 5.983 4.799z"
        />
        <Path
            style={props.selectedCountries.get('LA') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M650.745 466.397l-2.092 1.062-1.737 5.065 2.904 3.699-.485 4.09.485.196 4.832-2.342 6.482 7.243-.157 4.563 1.41.762 3.482-2.827-.285-2.238-10.053-9.552.095-1.461 1.254-.873-.874-2.438-4.158-.684-1.103-4.265z"
        />
        <Path
            style={props.selectedCountries.get('LB') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M487.139 440.041l.053 1.686-.708 2.56 2.438.208.156-3.631-1.939-.823z"
        />
        <Path
            style={props.selectedCountries.get('LC') ? props.countrySelectedStyle : props.selectedContinents.get('NA') ? props.continentSelectedStyle : props.countryStyle }
            d="M258.746 493.28l-.614 1.306.994 1.071 1.296-.691-1.676-1.686z"
        />
        <Path
            style={props.selectedCountries.get('LK') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M603.264 505.399l.217 2.351.216 1.712-1.271.216.64 3.848 1.909 1.07 2.966-1.711-.847-4.054.216-1.495-2.756-2.559-1.29.622z"
        />
        <Path
            style={props.selectedCountries.get('LR') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M378.198 515.027l9.491 6.345-.227-4.805-2.869-3.38-2.801-2.481-3.594 4.321z"
        />
        <Path
            style={props.selectedCountries.get('LS') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M470.896 606.829l2.637-2.032 1.245.053 1.503 1.875-.155 1.877-2.533.934v.728l-2.792-.156-.674-2.031.769-1.248z"
        />
        <Path
            style={props.selectedCountries.get('LT') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M452.14 375.236l-2.146.363.173 2.025 3.355.25 1.27 1.042.333 1.81 1.034 1.443 3.069-.13 2.938-3.743-.172-2.222-5.533-.867-4.321.029z"
        />
        <Path
            style={props.selectedCountries.get('LU') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M420.424 397.582l.76.679.88.083.194-1.734-.253-.974-1.224.583-.357 1.363z"
        />
        <Path
            style={props.selectedCountries.get('LV') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M462.823 369.964l-6.362-1.037-1.086 2.823-1.833.548-.959-1.173-.962-1.811-1.038.762-.589 3.132v1.708l2.242-.375 4.665.083 5.617 1.044 2.249-.657-.13-2.524-1.814-2.523z"
        />
        <Path
            style={props.selectedCountries.get('LY') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M429.958 453.518l1.35-.225.397-3.112h.674l2.758-4.528 6.804 1.979 1.856 2.887 6.69 3.062 3.482-1.47-.338-1.471-1.521-1.469.173-1.021 2.473-2.093h4.894l1.856 2.489 3.934.57.511 31.889-2.922-.112-17.651-9.181-1.91 1.081-7.253-1.814-1.971-2.603-2.87-.397-1.458-2.603.042-11.858z"
        />
        <Path
            style={props.selectedCountries.get('MA') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M402.505 439.903h-9.982l-1.954 4.339-4.504 2.17-3.719 10.062-7.243 4.341-10.174 16.761 9.983-.199.389-4.927h2.541v-6.708h8.81l.196-8.679 8.42-1.972 3.526-5.723 5.48-.198-1.769-9.267z"
        />
        <Path
            style={props.selectedCountries.get('MD') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M465.14 401.376l2.681 4.123-.226 2.334.96.043 2.272-3.847-2.731-3.389-1.547-.64-1.409 1.376z"
        />
        <Path
            style={props.selectedCountries.get('ME') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M449.68 416.677l-1.266 1.789.362 1.099 1.504.275 1.184-1.607-1.784-1.556z"
        />
        <Path
            style={props.selectedCountries.get('MG') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M526.988 561.474l-1.842 4.374-3.154 5.566-5.523.396-2.369 2.783.397 8.488-3.423 3.977.396 6.761 2.897 3.311 3.423-.396 3.423-2.524-.787-3.977 7.894-13.657-1.582-1.72 1.582-3.312 1.711.526.526-1.322-1.582-6.76-.924-2.784-1.063.27z"
        />
        <Path
            style={props.selectedCountries.get('MK') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M456.643 418.924l-2.912.959.139 2.473.683.873 3.458-1.606-1.368-2.699z"
        />
        <Path
            style={props.selectedCountries.get('ML') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M377.584 494.845l2.662-1.823 14.799-.087-3.423-23.806 3.907-.112 18.903 14.428 2.541.362-.959 8.021-11.886 1.08-9.171 6.847-1.669 4.686-6.37.269-1.625-4.677-4.884.346.189-1.53-3.014-4.004z"
        />
        <Path
            style={props.selectedCountries.get('MM') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M645.533 501.596l-2.396-3.838 1.737-2.438-1.642-3.018-1.548-.294-.294-5.064-2.316-4.486-.675 1.071-1.547 2.628-1.937.294-.968-1.271-.484-3.414-1.453-2.73-5.913-5.576 1.453-.959.27-4.037 2.16-3.631.935-9.032 3.129-2.136.103-3.293 1.877.622 2.956 4.279-2.194 4.701 1.479 3.691 3.655 1.435.666 4.021 4.91.761-1.357 2.343-6.188 2.438-.674 3.993 4.547 5.844.189 3.12-1.062 1.072.094.977 3.389 4.971.096 5.16-.997 1.796z"
        />
        <Path
            style={props.selectedCountries.get('MN') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M597.438 386.215l5.03-6.673 6.043 2.792 4.105 1.098 5.03-4.615-3.414-2.517 2.248-3.172 6.707 2.369 2.325 3.812 4.199.112 2.196-1.634 4.521-.182.985 1.678 7.512.38 4.754-4.849 6.578.69-.38 6.604 2.879.656 3.535-1.606 3.743 1.85-.088.935-2.714.078-2.827 5.93-2.195.216-8.54 11.16-8.723 3.847-5.455.424-4.529-2.923-5.791 3.095-5.705-1.771-1.617-4.141-10.805-.762-5.532-9.377-2.688-.174-1.387-3.33z"
        />
        <Path
            style={props.selectedCountries.get('MR') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M364.998 478.266l1.885 2.463-.389 10.65 2.74-1.972 1.952-.397 2.741.985 3.128 4.34 2.939-1.971 14.288-.199-3.526-23.866 3.786-.018-7.054-5.402.009 3.51-8.93.01-.043 6.698-2.567-.009-.328 4.944-10.631.234z"
        />
        <Path
            style={props.selectedCountries.get('MT') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M440.815 438.339l-1.443.294.052 1.6 1.297.433.579-.484-.485-1.843z"
        />
        <Path
            style={props.selectedCountries.get('MU') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M544.89 584.008l-1.312 1.721.259 1.857 2.768-2.256-1.715-1.322z"
        />
        <G style={props.selectedCountries.get('MV') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }>
            <Path d="M582.396 516.386l.26 2.256 1.442.527.26-1.989-1.962-.794zM584.238 521.156l-.13 2.784 1.055.525.925-1.856-1.85-1.453zM584.506 526.595l-.925.925 1.056.925 1.313-.925-1.444-.925z" />
        </G>
        <Path
            style={props.selectedCountries.get('MW') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M487.968 567.074l2.689 2.81-.053 3.597.52 1.514 3.57-3.855-.414-4.899-1.912-1.46-1.701-8.603-2.949-.104 1.34 6.196-1.09 4.804z"
        />
        <Path
            style={props.selectedCountries.get('MX') ? props.countrySelectedStyle : props.selectedContinents.get('NA') ? props.continentSelectedStyle : props.countryStyle }
            d="M133.847 433.982l4.175 13.146-1.945 1.089.216 2.61 3.674 2.827v5.229l4.538 4.356-1.945-12.847-2.593-8.497.648-5.877 2.161.217.865 1.962-.865 5.005 11.237 21.99v7.841l9.077 10.666 9.94 4.573 4.106-2.396 5.835 4.789 3.458-3.483-1.513-3.925 4.97-1.521 1.513.873 1.513-1.521h2.377l4.322-7.624-2.161-1.962-8.428 1.962-1.945 5.662-4.971.874-5.834-2.396-2.593-8.271 1.962-10.434-4.011-2.498-1.91-10.02-1.599-.683-2.922 2.965-3.354-1.789-1.313-6.682-13.286-1.393-6.863-5.16-6.536.343z"
        />
        <G style={props.selectedCountries.get('MY') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }>
            <Path
                className="prefix__mainland"
                d="M648.359 511.796l1.736 3.898.391 5.064 2.324 3.604 5.096 3.083 1-.791 1.464-.288-.212-1.91-1.841-4.478-2.697-5.731-.227 1.003-3.25-.146-2.334-3.354-1.45.046z"
            />
            <Path d="M675.527 526.896l2.61 3.018 10.012-3.467 1.979-7.643 4.46-.319 4.08-2.956-5.29-3.854-1.211-2.119-2.61 4.815.959 2.766-1.591 2.31-2.999-.771-7.27 5.333.188 3.086-3.317-.199z" />
        </G>
        <Path
            style={props.selectedCountries.get('MZ') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M482.791 596.359l2.326 1.928 5.479-3.335.881-4.953v-8.178l8.791-7.191 1.506.053 5.322-5.107-.828-10.529-13.81 1.743.519 3.338 2.02 1.757.57 5.731-4.756 4.642-1.141-2.603.207-3.44-2.74-2.973-6.725 3.13 6.258 3.182.209 9.274-4.141 6.146.053 7.385z"
        />
        <Path
            style={props.selectedCountries.get('NA') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M444.221 603.863l2.897.208 1.702 1.72 4.037.052.984-11.462v-7.503l2.585-.52.986-7.867 6.569-.206 2.323-1.927-3.933-.156-5.325.726-5.739-2.082h-16.13l.415 4.58 5.376 7.918-.934 4.062.053 2.136 4.134 10.321z"
        />
        <Path
            style={props.selectedCountries.get('NC') ? props.countrySelectedStyle : props.selectedContinents.get('OC') ? props.continentSelectedStyle : props.countryStyle }
            d="M798.706 602.576l-.303 1.547 3.983 5.559 2.145.926.303-2.161-6.128-5.871z"
        />
        <Path
            style={props.selectedCountries.get('NE') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M413.396 500.17l2.204-.053 1.988-2.981 3.336-.597 3.553 2.17 7.58.216 5.861-2.386 2.204-1.894.164-2.489 4.088-4.123 1.08-9.104-2.688-5.636-6.88-1.677-15.923 12.413-2.256-.217-.969 8.617-8.124.813 4.782 6.928z"
        />
        <Path
            style={props.selectedCountries.get('NG') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M413.984 515.185l3.389.164 4.088 4.557 1.987.544 1.558-.761 2.367-.328.805-3.303 3.225-2.117 3.492-.163 6.396-11.766-.104-2.653-2.955-2.273-5.913 2.603-7.909-.111-3.77-2.386-2.688.596-1.4 2.438-.104 6.88-2.256 3.198-.208 4.881z"
        />
        <Path
            style={props.selectedCountries.get('NI') ? props.countrySelectedStyle : props.selectedContinents.get('NA') ? props.continentSelectedStyle : props.countryStyle }
            d="M203.216 491.62l1.893.381.061 3.881-2.204 6.293-5.938-.588-1.323-3.034 1.764-3.682 3.345-3.112 2.402-.139z"
        />
        <Path
            style={props.selectedCountries.get('NL') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M421.349 384.572l-3.915 1.928.829.752.088 1.928-.829-.164-.917-1.426-2.188 3.467 3.363.699 1.253 1.323.666.017.44-2.99 2.117-.891-.907-4.643z"
        />
        <G style={props.selectedCountries.get('NO') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }>
            <Path
                className="prefix__mainland"
                d="M460.567 327.409l1.747-1.279-.157-1.435-1.106-.641.157-1.754h.95v-.96l-4.123-1.114-6.181.64-.631 2.714-1.428-.477-.951-1.599-3.017.155-.32 3.033-1.426.641-.795-1.6-6.345 5.109 1.271 1.436-2.378 1.115-5.393 10.701-1.901 1.279.155.959 1.9.959-.475 2.074-3.173-.164-.952-1.115-2.057 2.395-1.271.959-.32 2.24-1.105.64-2.854.64-1.426 4.479.951 7.348 1.106 3.354 1.271 1.279 2.853-.156 4.124-3.994 1.581-2.713.478 3.992 2.697-4.789.154-13.424 2.195-1.383.657-7.408 6.655-9.586 3.173-1.115 1.427-1.755 4.754 1.115 2.377 1.435.796-3.992 3.968-2.396 2.388 4.158z"
            />
            <Path d="M437.056 285.762l-1.426-1.435-3.164 1.539h-5.809l-.917 3.389 3.26 2.878 1.425-.208 2.04-3.491 1.729 1.235-1.229 2.464-.614 3.596 1.427 2.256 3.06-5.135 3.979-4.832-1.531-1.33-2.23-.926zM438.784 279.6l-2.55 2.359 1.529 2.359h2.749l1.124 1.539 3.363 1.746 3.871-2.256 2.654-2.256-.916-1.85-2.654-1.539-1.938 1.746-1.321-1.644-1.021.104-1.322 2.878-1.936-1.954-.208-1.331-1.424.099zM444.593 290.179l-2.04 1.851-1.729 1.332.812 1.435 1.636.511 2.652-1.236 1.229-1.539-1.124-1.85-1.436-.504z" />
        </G>
        <Path
            style={props.selectedCountries.get('NP') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M595.182 448.789l.397 3.691 6.983 3.162 11.193.83-.423-2.705-7.478-2.058-6.346-3.777-4.326.857z"
        />
        <G style={props.selectedCountries.get('NZ') ? props.countrySelectedStyle : props.selectedContinents.get('OC') ? props.continentSelectedStyle : props.countryStyle }>
            <Path d="M804.221 655.729l.917 10.199-1.228 4.634-4.6 3.396.305 4.02v4.322l1.228 1.548 12.577-10.814v-2.472h-3.068l-4.298-14.521-1.833-.312zM795.023 677.979l2.455 4.633-6.752 6.492-.613 3.396-4.6.613-7.667 7.104-7.054-3.396-.613-2.474 12.879-5.558 11.965-10.81z" />
        </G>
        <Path
            style={props.selectedCountries.get('OM') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M532.244 481.879l6.388-3.683 1.133-5.402-1.399-.804.579-5.792 1.219-.709 1.306 2.049 7.771 4.062v2.258l-9.413 13.854-4.331.147-3.253-5.98z"
        />
        <Path
            style={props.selectedCountries.get('PA') ? props.countrySelectedStyle : props.selectedContinents.get('NA') ? props.continentSelectedStyle : props.countryStyle }
            d="M205.68 506.748l-1.262 3.941 4.167 1.079 2.584.512.441-3.052 2.775-1.4 2.463 1.271.968 1.547 1.175-.138.925-2.81-3.078-1.271-2.334-1.271-2.333 1.59-2.775 1.4-2.835-1.141-.881-.257z"
        />
        <Path
            style={props.selectedCountries.get('PE') ? props.countrySelectedStyle : props.selectedContinents.get('SA') ? props.continentSelectedStyle : props.countryStyle }
            d="M209.518 541.246l-1.677 1.695.113 2.703 14.643 26.694 15.205 9.802 2.351-3.941.562-8.669-1.228-5.402-4.141-6.984-2.463.786-1.115 1.236-4.918-5.636 1.228-6.647 5.705-3.717-.449-3.492-5.809-.226-3.017-5.064-1.677-.562.113 3.044-7.486 8.895-5.593-1.349-.347-3.166z"
        />
        <G style={props.selectedCountries.get('PG') ? props.countrySelectedStyle : props.selectedContinents.get('OC') ? props.continentSelectedStyle : props.countryStyle }>
            <Path
                className="prefix__mainland"
                d="M752.132 540.183l-.319 21.126 3.044-.164 4.002-4.676 3.361.164 2.161 1.937.718 5.964 6.882 3.631 1.763-.648v-2.179l-5.523-4.599-2.723-6.293 2.161-1.047-1.601-3.466-3.197-.078-.805-3.709-8.479-5.722-1.445-.241z"
            />
            <Path d="M778.176 546.008l-.819.191-.501 2.222-1.573 1.021-4.729.83.19 1.779 4.979-.249 3.155-1.972-.188-3.432-.514-.39zM776.093 540.797l-.762 1.08 4.159 3.683.569 2.161 1.133-.13.13-2.221-1.263-1.14-3.966-3.433z" />
        </G>
        <G style={props.selectedCountries.get('PH') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }>
            <Path d="M697.337 496.306l-.743 1.418-.414 1.746-4.132 5.246.25 1.081 1.737-.251 5.368-5.999-2.066-3.241zM704.027 494.309l-.088 4.331 1.573 1.582.578 3.077 1.574.337.742-1.919-1.236-.916-.328-5.411-2.815-1.081zM708.496 495.978l-.087 3.829.908 1.495 1.571-1.832-.414-3.328-1.978-.164zM709.481 492.641l1.572 2.083.743 1.997h1.409l-.25-3.415-1.573-1.08-1.901.415zM712.542 500.472l.328 2.498-2.896 2.334-2.396.251-2.56 2.749.087 1.252 2.396-.751 1.651-1.08 1.408 3.578 2.48 1.747.994-.338.907-1.08-1.979-1.997 1.159-.916 1.322 1.08.906-1.495-.906-1.833-.164-4.08-2.737-1.919zM699.074 475.076l-2.23 1.581-.25 4.997 3.477 6.742 1.158.915 1.485-1.003 2.56.415.492 2.248 1.901.164.908-1.245-1.158-1.582-1.409-1.331-2.974-.327-1.573-2.585 1.816-2.749.163-2.411-1.236-3.077-3.13-.752zM700.232 489.979l.657 2.335 1.158.752.83-1.081-1.323-1.832-1.322-.174z" />
        </G>
        <Path
            style={props.selectedCountries.get('PK') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M553.638 455.082l2.248 3.337-.216 1.72-2.991 1.186-.217 2.801h3.424l1.175-.969h6.519l5.878 5.17.752-2.481h4.383l.104-3.12-4.486-4.305.96-2.369 4.598-.318 6.199-12.924-3.425-2.688-1.278-4.521 8.333-.752-4.917-7-2.62-.709-1.07 1.297-.805.061-4.919 3.12 1.608 2.696-1.815 1.937-2.249 8.29-5.558 3.553-.752 3.882-8.863 3.106z"
        />
        <Path
            style={props.selectedCountries.get('PL') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M457.109 390.184l.733 1.348.174 1.435-.604 1.392-1.383 2.664-1.167.526-1.514-.657-.908.043-2.204.83-2.506-.742-4.062-2.879-3.978-2.135-1.6-2.438-.303-5.75 3.112-2.705 4.062-1.35 1.328-.138.315 1.007 1.725.692 4.766.09 1.47-.043 2.421 3.708-.604 1.521.259 1.789.468 1.792z"
        />
        <Path
            style={props.selectedCountries.get('PR') ? props.countrySelectedStyle : props.selectedContinents.get('NA') ? props.continentSelectedStyle : props.countryStyle }
            d="M249.297 482.068l-2.282-.77-1.833 1.149.917 1.071 3.121.458.077-1.908z"
        />
        <G style={props.selectedCountries.get('PT') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }>
            <Path
                className="prefix__mainland"
                d="M387.499 421.716l-.536 7.478-1.53 1.384.156.846 1.071 1.772-.691 2.16 1.149.39 2.68-.312-.155-2.16 1.756-10.02-.382-1.383-3.518-.155z"
            />
            <Path d="M367.834 443.481l-.934 1.185.934 1.185 1.408-.709-1.408-1.661zM337.112 426.713l-1.175 1.184 2.107 1.185.234-1.649-1.166-.72zM343.448 426.004l-1.408.941 1.175.941 1.876-.476-1.643-1.406zM344.382 429.314l-.7 1.892.935 1.185 1.175-.941-1.41-2.136zM350.01 433.092l-.467 1.184.701.709 1.875-1.184-2.109-.709z" />
        </G>
        <Path
            style={props.selectedCountries.get('PY') ? props.countrySelectedStyle : props.selectedContinents.get('SA') ? props.continentSelectedStyle : props.countryStyle }
            d="M267.199 584.458l1.902 2.074-.225 4.392 5.48-.338 4.141 5.299-.337 4.729-2.681 4.054-5.479.225-.225-2.256 1.564-3.718-5.368-3.38h-4.469l-3.354-3.604 2.438-6.968 6.613-.509z"
        />
        <Path
            style={props.selectedCountries.get('QA') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M527.273 463.018l-.449 3.467 1.331 1.012 1.209-.112.45-4.365-1.047-.752-1.494.75z"
        />
        <Path
            style={props.selectedCountries.get('RO') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M457.731 401.281l-.226 1.279-5.005 4.166 4.184 6.137 2.682 1.877h4.823l1.591-1.331 2.135-.276 1.591.959 2.818-3.207-.545-1.607-2.861-.734-1.953-.095.094-2.749-2.593-4.08-6.735-.339z"
        />
        <Path
            style={props.selectedCountries.get('RS') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M452.001 407.279l-1.772 1.332h-.863l-.588 1.832 2.092 2.43.139 1.928-.882 1.246 3.068 3.197 3.317-1.012-.274-4.721-4.237-6.232z"
        />
        <G style={props.selectedCountries.get('RU') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }>
            <Path
                className="prefix__mainland"
                d="M722.059 302.16l1.522 5.256 3.043.873 3.042-4.814-1.737-3.285.648-2.845h4.563l-1.09 2.188.434 7.883-6.519 16.199.648 3.501-.216 5.912 12.161 17.729 2.387.657.216-14.443 2.386-2.188-2.61-5.688 2.171-2.412-4.78-6.346-2.611.217-.865-10.503 6.734-1.755.432-3.068 3.478-.873 1.953 1.756 2.386-9.631 4.124-7 3.259-1.756 2.827.217v-3.285l-4.563-.873-6.302-5.256 3.044-3.5-2.61-5.913 2.169-2.187 2.61 3.5 6.519 2.411 7.166.657.873-3.061-3.69-3.717 4.123-5.688-9.345-3.285-2.386 4.814-3.043-3.941-17.158-5.913-16.295 2.844-2.387 1.314v1.313l3.476 1.756-.434 4.157-6.301-2.628-13.898 5.473-2.388-5.031h-9.561l-4.349 4.599L669.894 263l-14.115 2.844-1.737 4.375 2.17.656-.216 3.285-13.685 1.529.873 4.375-12.604-2.188 3.044-5.688-12.819-.657 1.089 5.913-4.123 1.971-3.476-3.285-14.116 2.412-5.428 5.031-.217 3.06-3.476.216-.433-3.5 11.082-9.629v-6.57l-7.166-1.971-9.345 3.061-3.907-3.942h-1.737l-2.171 4.374 1.738 1.971-12.388 6.787-10.642 8.1-6.519 8.972v3.717l6.949 2.845-3.475 2.627-7.382-2.627-3.044 2.627-4.563-5.256-.873 1.972 4.996 15.758 1.305.44 3.478-1.754 1.736 1.313v2.844l-3.259-1.313-1.955 1.529 1.308 2.844-1.09 7.443-6.734.657-.432-2.412 3.907-2.411.873-6.57-4.349-5.688-1.521-9.846-6.948-1.098-.648 3.5 1.305 1.754-2.825 2.412 1.089 6.57 4.124 1.755.873 4.814-4.133-2.627-10.641-1.972-1.306 3.502-8.473 3.06-1.305-2.187-11.082 6.127-.216 4.158-4.348.657 1.306-3.06v-3.061l-4.349-1.529-2.826 1.098 2.386 4.599 1.737 3.06v2.412l-3.26-.656-.647-.657-3.26 3.501 1.737 3.061-7.383-.217 2.387 3.069-.647 1.313h-3.907l-2.827-1.971-.647-5.472-4.563-1.755v-2.187l9.561 1.972 5.213.44 2.17-3.285-1.954-3.501-13.899-5.472-4.798 1.192-1.642 1.41.511 3.24 2.039.354-.476 5.101 6.293 14.781-4.548 7.209-.312 1.625 2.309 1.625-2.084 1.375-1.383.026.26 6.353 1.91 2.706.026 2.627 2.446.225 3.741 1.426 3.959 5.446.045 1.435-1.288 2.205 2.956-.164 2.878.83 3.891 5.506 9.577.873-.415 6.552-3.301 2.827.683 1.105-3.26 3.502-.864 3.284 1.954 2.845 6.301 2.187 2.611-1.53 16.727 6.346.648-1.756-3.476-3.283v-4.158l-2.171-.657.434-3.5 3.476-4.158-6.231-4.667.432-6.492 6.665-4.383 7.822.441 1.306 2.412 8.04.44 5.869-3.284-3.044-3.285.647-6.129 15.205-7.442 11.695 5.272 3.907-3.5 11.514 10.942 8.688-.873 3.044 3.06 8.255.873 5.429-7.441 6.949 3.068 3.69.658 3.692-3.285-3.26-2.188 2.827-4.374 8.039 2.628 1.736 3.501 3.477.216 2.17-1.529 5.869-.217.647 1.53 6.733.44 4.562-4.814 9.345 1.098 2.827-1.098.864-5.256-2.826-6.346 2.826-2.411h8.904l8.471 10.069 10.857 6.129h3.26l.432-2.627 3.907-2.412.433 14.228-3.475.216v3.5l1.953 2.412-.363 3.129 1.443.598.874-2.188 1.306.441.864.873 3.907-.873 3.906-11.385.434-14.229-4.996-11.385-6.301-7.657-3.044.44v2.412l-7.382-2.845 2.826-6.128 2.387-16.199 9.992-3.06 4.779-3.06h5.213l-1.312 1.754 1.306 2.188 4.563-4.814 2.609.215-.432-2.844-4.132-.873 2.827-10.287 3.737-3.523z"
            />
            <Path d="M450.108 378.288l-1.296 2.396 4.665.043h.95l-.179-1.352-.728-.854-3.412-.233zM741.137 353.246l-1.071 1.332.087 2.083.992-.086 1.651-2.913-1.659-.416zM776.793 272.303l-2.04 1.331-.483 1.694.96 1.089 2.16-.726 2.161.726 1.201.363-.121-3.994-3.838-.483zM488.539 272.648l1.487.598-1.046 1.798v2.55l-2.23 1.35h-2.377l-1.34-1.651.146-1.798 1.046-1.35h2.084l2.23-1.497zM494.192 270.998v1.798l1.486 1.202 2.083-.146 1.788-1.651v-1.202h-1.634l-1.34.449-1.046-1.201-1.337.751zM502.681 271.152l1.046 2.248 2.084.147 1.486-.596-.742-2.101-1.937-.451-1.937.753zM511.161 268.154l-1.635-.303-1.487 1.504.744 1.349.449 2.101 1.937-1.496.449-1.65-.457-1.505zM520.237 284.051l-.45 2.1-3.424 3-7.294 1.651-5.957 9.897-1.046 2.853 5.957 1.505.889-3.597 1.79-5.549 4.615-2.403 3.872-3 2.826-1.201h1.487v-4.046l-3.265-1.21zM501.039 305.946l4.019.45 1.342 4.649 3.423 3.597-1.193 2.402h-2.083l-1.937-2.248-4.313-.146-1.789-2.403v-1.651l2.682-.752-.151-3.898zM563.855 254.809l-1.938-1.203h-2.23l-.448 1.35-2.377 1.35-1.79.596-.294 1.798 4.167.303 4.91-4.194zM568.463 255.257l-1.047 2.247-2.083-.146-3.276 2.402-.89 3h2.083l1.193-1.953 2.826 2.101 2.681-1.202 1.937-1.65-.744-2.551-1.046-1.798-1.634-.45zM572.784 256.908l1.046 4.201 1.634 3.897 1.79-3.146 3.423-.752v-2.248l-2.229-1.651-5.664-.301zM654.453 250.184l2.326 1.953 1.649-.683.484-2.74-3.389-2.342-2.23 1.469-5.428.493v2.445l-5.724.096v4.002l6.69 4.979 1.747-1.271-.39-3.519 4.27-1.071-.873-1.66-1.547-1.563 2.415-.588zM660.66 247.84l1.547 2.932 6.017-.684 1.65-2.152-.389-1.857-1.65-.684-1.548 1.176-4.46.978-1.167.291zM660.271 259.268l-3.01-.777-1.736 1.857-.778 2.541 4.071-.389 3.104-1.564-1.651-1.668zM738.231 242.369l-2.523-.778-2.904 1.072-1.453 2.151 1.842 2.446 4.85-2.151.968-1.072-.78-1.668zM739.156 358.329v3.665l1.159.415.828-1.332v-2.827l-1.987.079zM705.35 345.086l-.076 5.333 6.689 10.33 2.396 8.989 4.219 7.996 1.649.58 1.409-1.168.657-1.918-6.033-6.578.164-3.416 1.322-.578.329-1.997-11.816-16.735-.909-.838zM751.967 328.516l-1.649.164.993 1.418 2.065 1.418.579-.666-1.988-2.334zM755.183 329.52l.251 1.416 2.56.752.251-1.002-3.062-1.166zM769.229 334.956l1.08 1.937 1.8-.121.361-1.332-3.241-.484zM787.356 337.98l1.442 2.662 1.08-1.209v-1.815l-2.522.362z" />
        </G>
        <Path
            style={props.selectedCountries.get('RW') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M479.896 532.931l2.429 2.238-.104 2.396-3.769.077v-2.646l1.444-2.065z"
        />
        <Path
            style={props.selectedCountries.get('SA') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M519.812 458.021l6.061 8.443 1.953 1.558.874 3.785 9.327.734 1.055.554-1.046 4.667-6.129 3.613-8.964 2.715-4.78 4.668-5.679-3.312-3.439 3.009-4.791-7.823-3.284-1.504-1.192-1.807v-3.916l-11.954-14.452-.451-2.561h3.44l4.184-3.611.146-1.808-1.192-1.201 2.395-1.953 5.084.303 8.669 7.226 5.117-.232.329 1.263 4.267 1.642z"
        />
        <G style={props.selectedCountries.get('SB') ? props.countrySelectedStyle : props.selectedContinents.get('OC') ? props.continentSelectedStyle : props.countryStyle }>
            <Path d="M783.786 549.882l1.072 2.981 1.892 1.842.57-.51-.189-1.972-2.145-2.603-1.2.262zM789.016 554.324l.131 1.971 1.2 1.141 1.134-.7-1.012-2.102-1.453-.31zM790.528 559.218l-1.012 1.081 1.071 1.97 1.262.381-.06-1.331-1.261-2.101zM792.992 558.076l.882 2.161 1.702 2.03.943-1.521-1.263-2.161-2.264-.509zM797.409 561.317l.501 2.671 1.203 1.65 1.011-2.092-2.715-2.229zM798.792 567.291l-.44.76 1.452 1.911 1.012.062-.632-2.482-1.392-.251zM795.576 571.094l-1.514.7 1.323 1.843 1.133-.64-.942-1.903z" />
        </G>
        <G style={props.selectedCountries.get('SC') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }>
            <Path d="M535.676 548.87l-.525 1.062 1.442 1.192 1.056-1.192-1.973-1.062zM543.049 540.919l-1.582 1.062 1.186 1.857h1.582l-1.186-2.919zM543.706 545.56l-1.055 1.193.786 1.192 1.442.269.13-2.522-1.303-.132z" />
        </G>
        <Path
            style={props.selectedCountries.get('SD') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M466.144 505.035l-2.55-1.504-2.325-4.59.13-4.271 3.224-2.772.155-10.228 2.127.062-.242-5.68 22.302.198 3.189-3.215 6.881 11.004-3.77 4.442v6.785l-4.599 9.891-1.04 2.3-3.709-5.316-2.708 3.441-3.059.834-9.941-1-4.333 1.541.268-1.922z"
        />
        <G style={props.selectedCountries.get('SE') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }>
            <Path
                className="prefix__mainland"
                d="M445.232 329.52l1.693 1.563h3.173l1.746 3.354.477 5.748-4.278 3.035v3.033l-3.017 4.158-1.746.154-2.378 3.994.155 3.838 4.124 3.035-.319 1.754-1.582 2.396-2.377 2.074.155 6.872-3.647 1.279-1.271 2.713h-1.747l-.949-4.789-3.969-6.084 3.26-5.455.225-13.477 2.248-1.236.545-7.709 6.405-9.172 3.074-1.078z"
            />
            <Path d="M445.898 368.927l-1.824 1.443.917 2.118 1.616-1.573-.709-1.988z" />
        </G>
        <Path
            style={props.selectedCountries.get('SG') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M658.314 527.705l.686.389 1.548-.126-.13-1.167-1.262.199-.842.705z"
        />
        <Path
            style={props.selectedCountries.get('SI') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M442.708 405.076l-2.195 1.314-4.097.898.82 2.368 2.87.034 2.646-2.213-.044-2.401z"
        />
        <Path
            style={props.selectedCountries.get('SK') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M443.607 400.875l.597.527.077.898 6.596-.146 4.875-2.102-.077-2.135-.934.415-1.34-.718-.821-.035-2.161.865-2.938-.709-3.874 3.14z"
        />
        <Path
            style={props.selectedCountries.get('SL') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M372.804 509.79l4.884 4.72 3.483-4.227-2.179-3.415-3 .303-3.188 2.619z"
        />
        <Path
            style={props.selectedCountries.get('SN') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M372.424 498.771l-5.792-.141 1.072 2.603.596-1.607 7.271.761.806-.028 3.405.12.121-1.504-3.111-3.733-3.467-4.693-2.152-.898-1.661.424-3.405 2.472-.776 1.384-.242 1.383 1.253.899 4.185-.061 2.688-.728.303 1.322-.241 1.746-.853.279z"
        />
        <Path
            style={props.selectedCountries.get('SO') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M526.703 501.941l3.777-1.452 1.34.804-.147 3.354-3.482 9.923-18.854 20.19-2.187-1.503-.147-8.521 2.835-3.26 6.018-1.857 8.824-9.318 2.31-2.058.647-3.008-.934-3.294z"
        />
        <Path
            style={props.selectedCountries.get('SR') ? props.countrySelectedStyle : props.selectedContinents.get('SA') ? props.continentSelectedStyle : props.countryStyle }
            d="M268.384 516.715l1.763 1.616 2.731-1.694 2.49.078-.32.968-1.046 2.179-.164 5.421-4.97 2.022.242-3.476-3.207-2.991.164-1.538 2.317-2.585z"
        />
        <Path
            style={props.selectedCountries.get('SS') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M489.336 508.019l-2.04.898.647 3.553h2.542l3.448 5.004-2.767.354-.709 1.288-.069 1.857-8.298-.146-.848-1.288-5.8-.328-10.649-10.962 1.063-.64 4.517-1.364 9.897.754 3.366-.754 2.235-2.996 3.465 4.77z"
        />
        <G style={props.selectedCountries.get('ST') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }>
            <Path d="M421.911 530.554l.993-.502.743.604-.743 1.148-.899-.354-.094-.896zM423.907 527.398l1.496-.251.501.951-.743.805-.743-.104-.511-1.401z" />
        </G>
        <Path
            style={props.selectedCountries.get('SV') ? props.countrySelectedStyle : props.selectedContinents.get('NA') ? props.continentSelectedStyle : props.countryStyle }
            d="M189.308 495.217l4.062 2.022-.061-3.207-2.083-1.271-1.918 2.456z"
        />
        <Path
            style={props.selectedCountries.get('SY') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M487.545 437.18l-.302 2.196 2.437 1.021-.104 6.086 2.438-.053 2.438-1.842.916-.155 5.532-4.398 1.114-6.39-11.056 1.125-1.167 2.559-2.246-.149z"
        />
        <Path
            style={props.selectedCountries.get('SZ') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M482.531 596.983l-2.169.361-.935 2.552 1.659 1.513h2.015l1.702-2.446-2.272-1.98z"
        />
        <Path
            style={props.selectedCountries.get('TD') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M440.971 494.983l.112-2.552 4.098-3.983 1.099-9.785-2.731-5.221 1.911-.979 18.498 9.639-.113 9.456-3.259 2.775v4.875l2.136 4.132h-3.77l-6.24 6.173-.165 1.867-4.605-.061-.061.846-2.628-.345-1.798-3.397-1.351-.666.174-1.037 1.693-1.297v-6.066l-2.343-.363-2.826-2.102 2.169-1.909z"
        />
        <Path
            style={props.selectedCountries.get('TG') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M408.495 516.81l2.316-1.356-.053-8.946-1.504-2.438-.967.812.208 11.928z"
        />
        <Path
            style={props.selectedCountries.get('TH') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M646.043 472.915l2.8 3.604v4.384l.968.482 4.452-2.144.873.294 5.316 6.138-.19 4.192-1.737-.294-1.548-.979-1.158.097-2.031 3.404.39 1.851 1.642.873-.095 2.049-1.157.588-3.97-2.731v-2.438l-1.642-.095-.674 1.07-.347 10.909 2.567 4.686 4.547 4.383-.188 1.271-2.423-.094-2.221-3.311h-2.325l-2.902-2.345-.874-2.437 1.254-2.049.432-1.851 1.366-2.421-.061-5.565-3.337-4.823-.139-.588 1.081-1.089-.251-3.83-4.441-5.627.519-3.241 5.504-2.323z"
        />
        <Path
            style={props.selectedCountries.get('TJ') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M559.74 422.234l3.552-4.408h1.34l.467.984-1.642 1.192v.985l1.081.777 5.195.312 1.693-.727.77.154.52 1.66 3.085.312 1.548 3.267-.467.985-.614.053-.612-1.245-1.341-.104-2.315.312-.156 2.18-2.316-.155.104-2.75-1.694-1.658-2.575 2.125.053 1.4-2.265.778h-1.341l.104-4.824-2.174-1.605z"
        />
        <Path
            style={props.selectedCountries.get('TM') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M528.328 418.561l-.535 2.273h-3.588v3.078l3.854 2.541-1.191 3.482v1.608l1.599.269 2.127-2.811 4.789-1.07 10.234 3.881.129 2.809 5.714.536 6.38-6.698-.796-2.145-4.253-.935-11.964-7.771-.535-2.81h-4.521l-1.997 3.753h-1.997l-3.449.01z"
        />
        <Path
            style={props.selectedCountries.get('TN') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M425.516 435.624l4.78-1.927 1.572 1.02.061 1.244-.734.959.111 1.703.735.396v3.061l-.847 1.418.111.908 3.207 1.132-2.584 4.02-1.012-.061-.173 3.232-1.124.174-.959-.849.225-3.284-3.146-3.061-.397-2.662 1.521-1.192-1.347-6.231z"
        />
        <Path
            style={props.selectedCountries.get('TR') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M472.812 421.906l-2.305-1.426-1.271-1.013-2.138.916-1.477 3.74 2.219-.5 1.562-1.188 3.438.938-1.946 1.877-5.175-.25-1.91 2.093v1.021l1.22 1.021v1.123l-.511 1.332.511 1.123 1.625-.812 1.625 1.737-.406 1.228-.604.82.907 1.021 4.461.916 3.139-1.331v-1.937l1.521.303 3.648 2.144 3.948-.614 1.721-1.633 1.114.406v1.841h1.521l1.313-2.55 11.549-1.229 5.04-.613-1.331-1.746-.025-2.359 1.011-1.21-3.682-2.956.197-2.551h-2.022l-3.354-1.643-1.929 2.041-7.088-.209-4.253-2.549-4.082.366-4.544 2.729-3.237-.417z"
        />
        <Path
            style={props.selectedCountries.get('TT') ? props.countrySelectedStyle : props.selectedContinents.get('NA') ? props.continentSelectedStyle : props.countryStyle }
            d="M258.97 502.572l-.917.847-.994.155v1.228l1.833 1.687.76-1.229.458-1.383-.156-1.149-.984-.156z"
        />
        <Path
            style={props.selectedCountries.get('TW') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M695.686 453.76l-3.06 2.334-.163 4.494 2.646 3.078.656-.58-.079-9.326z"
        />
        <Path
            style={props.selectedCountries.get('TZ') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M492.22 560.017l13.797-1.69-3.395-6.569-.182-6.292 1.098-3.009-14.367-9.023-4.502.743-1.564 1.158-.139 2.637-1.012 3.656-1.055 1.253-1.514.141 2.809 9.418 4.816 2.838 4.195.1 1.015 4.639z"
        />
        <Path
            style={props.selectedCountries.get('UA') ? props.countrySelectedStyle : props.selectedContinents.get('EU') ? props.continentSelectedStyle : props.countryStyle }
            d="M460.662 388.791l-2.507 1.409.622 2.663-2.316 4.884.02 2.151 1.089.691 6.983.346 1.954-1.615 2.092.699 2.999 4.002-2.194 3.942 2.61.761 3.414-3.933 1.954.354 1.815 1.262-1.601 2.109 2.161 3.371h2.299l1.185-2.248 2.438-.494.069-1.823-4.529-.7.14-1.963h4.391l4.737-3.794 2.092-1.824.345-5.757-9.336-.838-3.829-5.402-2.646-.908-3.207.139-1.443 3.569-6.57.087-2.135-.986-3.096-.154z"
        />
        <Path
            style={props.selectedCountries.get('UG') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M480.311 532.23l2.619 2.454 1.643-1.045 4.442-.728.762.079.284-1.688 2.508-5.272-2.109-4.392-6.837.044-.043 1.808.917.882-.139 1.807-4.047 6.051z"
        />
        <G style={props.selectedCountries.get('US') ? props.countrySelectedStyle : props.selectedContinents.get('NA') ? props.continentSelectedStyle : props.countryStyle }>
            <Path
                className="prefix__mainland"
                d="M143.589 375.989l-.865 3.475-3.017-1.954h-1.504l-.865 3.691-10.554 23.65 2.801 20.606 3.449 1.737.648 5.645h7.105l6.889 5.204 13.562 1.305 1.504 6.941 2.152 1.521 3.017-3.033 2.369 1.08 2.152 9.976 3.656 2.386 3.017-5.645 9.258-6.726 6.025 2.817 5.169.433.216-3.25 10.762.217 2.152 2.386.432 5.42-1.288 3.034 1.504 5.203h3.233l3.232-4.987-1.288-2.386-1.288-5.204 1.936-5.86 8.826-7.59 6.673-1.953-.864-6.293 9.258-9.983 9.258-1.521-1.504-5.193 9.042-5.205v-6.94l-.865-.433-3.233 1.082-.432 4.252-10.745.129-8.419 5.594-13.217 4.322-2.109-2.586 5.999-9.076-2.965-2.826-2.014-3.838-4.175-3.354-4.538-.38-8.575-5.852-60.972-10.038z"
            />
            <Path d="M74.791 285.234l2.991 5.594 1.919-.432v-1.938l-4.91-3.224zM57.926 334.428l-.147 2.602 1.867-.432v-1.158l-1.72-1.012zM55.057 335.586l-3.734 1.885.579 2.022 1.435-1.158 2.87-1.306-1.15-1.443zM39.541 338.042l-2.584-.579-.432 1.158.285 2.169 2.731-2.748zM34.078 337.902l-2.446-1.01-.865 1.59 1.582 1.591 1.729-2.171zM95.485 277.922l-7.252 1.721 1.496 8.168 7.892 2.151.423 1.72-11.73 3.657-6.613 10.96 2.343 11.609 3.838 2.576 2.991-2.793.856 1.721-3.63 4.296-14.082 6.449-8.964 2.151-.216 3.225 20.694-6.016 8.532-2.369 7.892-9.674 8.748-5.8-4.478 7.521 4.91.647 8.324-3.656 1.496 6.017 5.757 1.288 5.973 5.8.423 4.296-.855 1.072 1.063 4.08h1.496l.216-6.881h1.703l.423 16.977 4.271-3.657-2.991-17.625h-4.478l-4.91-6.231 24.108-40.844-23.892-18.696-26.667 5.161-1.063 8.168 5.757 3.439-2.135 5.594-7.669-6.222z" />
        </G>
        <Path
            style={props.selectedCountries.get('UY') ? props.countrySelectedStyle : props.selectedContinents.get('SA') ? props.continentSelectedStyle : props.countryStyle }
            d="M274.633 612.481l-1.773 1.894.735 10.183 5.566 1.615 7.08-7.097-11.608-6.595z"
        />
        <Path
            style={props.selectedCountries.get('UZ') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M558.643 428.477l2.662.138v-4.556l-2.522-1.47 4.253-5.358h1.729l1.729 2.015 4.521-1.738-6.25-2.144-.242-1.297-1.485.363-1.461 2.541-6.302-.207-4.625-6.543-8.125.803-3.872-3.838-5.358-.906-3.891 1.582 2.257 7.502.024 2.524 1.643.035 2.014-3.839 5.359.068.796 2.947 11.487 7.625 4.442 1.021 1.217 2.732z"
        />
        <G style={props.selectedCountries.get('VC') ? props.countrySelectedStyle : props.selectedContinents.get('NA') ? props.continentSelectedStyle : props.countryStyle }>
            <Path d="M258.823 496.582l-1.063.77.839 1.539 1.374-.77-1.15-1.539zM257.526 499.573l-.994.994.38.612h1.219l.38-1.003-.985-.603z" />
        </G>
        <Path
            style={props.selectedCountries.get('VE') ? props.countrySelectedStyle : props.selectedContinents.get('SA') ? props.continentSelectedStyle : props.countryStyle }
            d="M231.5 503.558l.38 2.239 2.809.892.64-4.124 2.965-3.068 2.965 3.475 6.82 1.859 5.774-1.211 3.933 4.849 2.965 1.858-3.25 4.953 1.089 3.752-1.858 2.299-1.928 1.616-4.175-2.102-.959.969v2.99l3.052 1.452-2.248 2.43-2.248 2.43-2.965-.242-2.982-3.275-.631-12.326-10.183-3.476-1.85-5.42 1.885-2.819z"
        />
        <Path
            style={props.selectedCountries.get('VN') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }
            d="M659.035 502.287l1.027 1.616.19 1.851 2.705.294 3.286-4.383 3.095-.873 1.643-4.478-.771-7.209-3.189-4.384-3.362-2.688-4.278-7.349 3.068-5.135-4.393-5.039-3.517-.154-3.164 1.702.942 4.071 4.219.743 1.133 3.138-1.487.969.096.777 9.896 9.683.388 2.844-.595 8.989-6.932 5.015z"
        />
        <G style={props.selectedCountries.get('VU') ? props.countrySelectedStyle : props.selectedContinents.get('OC') ? props.continentSelectedStyle : props.countryStyle }>
            <Path d="M811.006 582.479l-1.071 1.435.45 1.616.535.362.979-1.262-.893-2.151zM811.542 586.879l.087 1.167 1.158.363.805-.449-.805-1.264-1.245.183zM813.236 597.303l-.536.812.804.897 1.34-.448-1.608-1.261z" />
        </G>
        <G style={props.selectedCountries.get('YE') ? props.countrySelectedStyle : props.selectedContinents.get('AS') ? props.continentSelectedStyle : props.countryStyle }>
            <Path
                className="prefix__mainland"
                d="M509.432 489.131l1.244 3.7v3.613l2.991 2.714 21.074-8.584.198-2.359-3.381-6.067-8.479 2.706-4.866 4.787-5.645-3.336-3.136 2.826z"
            />
            <Path d="M533.315 498.138l1.842 2.059 2.489-1.504.897-.304-1.141-1.105-2.188.647-1.899.207z" />
        </G>
        <Path
            style={props.selectedCountries.get('ZA') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M476.731 588.02l-6.829 6.311-1.625 3.898-5.411-.674-4.503 4.002-2.991-.294.241-5.531-1.062-.372-.743 11.314-5.308-.052-1.6-1.886-2.344-.024 2.137 6.129 3.812 3.604-2.723 3.172 1.764 3.977 4.08 1.558 3.25-2.767 9.311.053.667-.83 4.132-.728 13.978-13.917-.053-4.382-1.495 1.937h-2.238l-2.723-2.282 1.383-3.44 2.378-.482-.217-7.071-5.268-1.223zm-3.276 15.991l1.306-.052 2.118 2.299-.061 2.662-2.481 1.253-.155.883-3.785.043-1.186-2.853 1.081-2.092 3.163-2.143z"
        />
        <Path
            style={props.selectedCountries.get('ZM') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M459.78 571.656l2.739 3.802 4.244.261 1.504.829 4.443.053 3.829-5.367 10.702-4.789.934-4.219-1.244-6.043-5.584-3.181-3.727.26-1.857 4.114.053 1.876 4.391 2.136.26 4.642-3.775.208-.935-1.564-10.495-4.479-.311 3.44-4.962.155-.209 7.866z"
        />
        <Path
            style={props.selectedCountries.get('ZW') ? props.countrySelectedStyle : props.selectedContinents.get('AF') ? props.continentSelectedStyle : props.countryStyle }
            d="M468.52 578.226l7.755 8.757 5.946 1.513 3.984-6.248-.312-8.281-6.465-3.337-2.431 1.098-3.62 5.524-5.014-.053.157 1.027z"
        />
    </Svg>
);

export default SvgComponent;
