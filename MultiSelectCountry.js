import React from 'react';
import { SectionList, TouchableHighlight, Text, View } from 'react-native';
import sectionListGetItemLayout from 'react-native-section-list-get-item-layout';
import { Flag } from 'react-native-svg-flagkit';

class ListItemCountry extends React.PureComponent {
    _onPress = () => {
        this.props.onPressItem(this.props.code, this.props.continentCode);
    };

    render() {
        const style = this.props.selected ? [this.props.listItemStyle, this.props.listItemSelectedStyle] : this.props.listItemStyle;
        const textStyle = this.props.selected ? [this.props.listItemTextStyle, this.props.listItemSelectedTextStyle] : this.props.listItemTextStyle;

        return (
            <TouchableHighlight
                onPress={this._onPress}
                style={style}
                underlayColor={this.props.listItemHighlightColor}
            >
                <>
                    <Text style={textStyle}>{this.props.name}</Text>
                    <View style={this.props.listItemFlagStyle}>
                        <Flag id={this.props.code} width={30} height={30} />
                    </View>
                </>
            </TouchableHighlight>
        );
    }
}

export default class MultiSelectCountry extends React.PureComponent {
    constructor(props) {
        super(props);

        // Specifying item height dramatically increases performance of SectionList
        // https://medium.com/@jsoendermann/sectionlist-and-getitemlayout-2293b0b916fb
        // https://www.npmjs.com/package/react-native-section-list-get-item-layout
        this.getItemLayout = sectionListGetItemLayout({
            getItemHeight: () => this.props.listItemStyle.height + this.props.listItemStyle.borderBottomWidth,
            getSectionHeaderHeight: () => this.props.listHeaderStyle.height,
        });
    };

    _renderSectionHeader = ({section: {title}}) => (
        <View style={this.props.listHeaderStyle}>
            <Text style={this.props.listHeaderTextStyle}>{title}</Text>
        </View>
    );

    _renderItem = ({item, index, section}) => (
        <ListItemCountry
            name={item.name}
            code={item.code}
            continentCode={section.code}
            selected={!!this.props.selected.get(item.code)}
            onPressItem={this.props.onPressItem}
            listItemStyle={this.props.listItemStyle}
            listItemTextStyle={this.props.listItemTextStyle}
            listItemFlagStyle={this.props.listItemFlagStyle}
            listItemHighlightColor={this.props.listItemHighlightColor}
            listItemSelectedStyle={this.props.listItemSelectedStyle}
            listItemSelectedTextStyle={this.props.listItemSelectedTextStyle}
        />
    );

    render() {
        return (
            <SectionList
                sections={this.props.countrySections}
                extraData={this.props}
                keyExtractor={(item, index) => item.code}
                renderSectionHeader={this._renderSectionHeader}
                renderItem={this._renderItem}
                getItemLayout={this.getItemLayout}
            />
        );
    }
}
