import { Dimensions } from 'react-native';

export const width = Dimensions.get('window').width;
export const height = Dimensions.get('window').height;

// vw/vh are 1% of the screen width/height respectively
export const vw = width / 100;
export const vh = height / 100;

export const vmin = Math.min(vw, vh);
export const vmax = Math.max(vw, vh);
