import React from 'react';
import { StyleSheet, View, SafeAreaView } from 'react-native';

import { vw, vh } from './ViewPortUnits';

import MapSvg from './MapSvg';
import countriesByContinent from './MapCountries';

import MultiSelectCountry from './MultiSelectCountry';

export default class App extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            selectedCountries: new Map(),
            selectedContinents: new Map(),
        };
    };

    _onPressItem = (countryCode, continentCode) => {
        this.setState(previousState => {
            const selectedCountries = new Map(previousState.selectedCountries);
            const selectedContinents = new Map(previousState.selectedContinents);
            const previouslySelected = selectedCountries.get(countryCode);

            selectedCountries.set(countryCode, !previouslySelected);

            if (previouslySelected) {
                selectedContinents.set(continentCode, (selectedContinents.get(continentCode) || 0) - 1);
            } else {
                selectedContinents.set(continentCode, (selectedContinents.get(continentCode) || 0) + 1);
            }

            return {
                selectedCountries,
                selectedContinents,
            };
        });
    };

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.mapContainer}>
                    <MapSvg
                        countries={countriesByContinent.reduce((allCountries, continent) => allCountries.concat(continent.countries), [])}
                        selectedCountries={this.state.selectedCountries}
                        selectedContinents={this.state.selectedContinents}
                        countryStyle={mapStyles.country}
                        countrySelectedStyle={mapStyles.countrySelected}
                        continentSelectedStyle={mapStyles.continentSelected}
                        style={styles.mapSvg}
                    />
                </View>

                <View style={styles.listContainer}>
                    <MultiSelectCountry
                        countrySections={countriesByContinent.reduce((sections, continent) => [...sections, { title: continent.name, code: continent.code, data: continent.countries }], [])}
                        selected={this.state.selectedCountries}
                        onPressItem={this._onPressItem}
                        listHeaderStyle={styles.listHeader}
                        listHeaderTextStyle={styles.listHeaderText}
                        listItemStyle={styles.listItem}
                        listItemTextStyle={styles.listItemText}
                        listItemFlagStyle={styles.listItemFlag}
                        listItemHighlightColor={styles.listItemHighlight.color}
                        listItemSelectedStyle={styles.listItemSelected}
                        listItemSelectedTextStyle={styles.listItemSelectedText}
                    />
                </View>
            </SafeAreaView>
        );
    }
};

const theme = {
    blue: '#43a2ca',
    green: '#a8ddb5',
    lightGreen: '#e0f3db',
    gray: '#ddd',
    white: '#fff',
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: theme.white,
    },
    mapContainer: {
        flex: 1,
        width: 100*vw,
        height: 56.25*vw, // 16:9 ratio
        maxHeight: 30*vh, // Make sure map doesn't take more than 30% of the available vertical space
        marginBottom: 15,
    },
    mapSvg: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
    },
    listContainer: {
        flex: 1,
    },
    list: {
    },
    listHeader: {
        height: 50,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 20,
        backgroundColor: theme.gray,
    },
    listHeaderText: {
        fontSize: 21,
        fontWeight: 'bold',
    },
    listItem: {
        height: 70,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        borderBottomWidth: 1,
        borderBottomColor: theme.gray,
    },
    listItemHighlight: {
        color: theme.gray,
    },
    listItemText: {
        fontSize: 21,
        flexShrink: 1,
    },
    listItemFlag: {
        flexGrow: 0,
        marginLeft: 25,
    },
    listItemSelected: {
        backgroundColor: theme.blue,
        borderBottomColor: theme.white,
    },
    listItemSelectedText: {
        color: theme.white,
    },
});

const mapStyles = {
    country: {
        fill: theme.lightGreen,
    },
    countrySelected: {
        fill: theme.blue,
    },
    continentSelected: {
        fill: theme.green,
    },
};
